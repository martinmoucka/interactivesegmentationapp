#include <iostream>
#include <wx/wx.h>
#include "MainFrame.h"


class MyApp : public wxApp {
public:
    virtual bool OnInit();
    virtual int OnExit();
};

IMPLEMENT_APP(MyApp)


bool MyApp::OnInit() {
    MainFrame *f = new MainFrame(NULL);
    f->Show(true);
  
    return true;
}

int MyApp::OnExit() {
    return this->wxApp::OnExit();
}


