//
//  SegmentedImage.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 08.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef SegmentedImage_hpp
#define SegmentedImage_hpp

#include <stdio.h>
#include <i3d/image3d.h>
#include <i3d/draw.h>
#include <wx/wx.h>
#include "Util.hpp"


// Class that is used to represent the image that is being currently segmented.

class SegmentedImage {
    i3d::ImgVoxelType _type; // i3d::Gray8Voxel or i3d::RGBVoxel
    i3d::Image3d<i3d::GRAY8> _originalGrayImage; // If type == i3d::Gray8Voxel, this variable stores original image
    i3d::Image3d<i3d::RGB> _originalRgbImage; // If type == i3d::RGBVoxel, this variable stores original image
    i3d::Image3d<i3d::GRAY16> _labeledImage; // Labeled image - result of segmentation
    wxBitmap _originalBitmap; // Used to draw on screen
    wxBitmap _labeledBitmap; // Used to draw on screen. Computed when labels are updated
    wxBitmap _labeledOutlinedBitmap; // Used to draw on screen. Computed when labels are updated
    size_t _width; // width if the image
    size_t _height; // height of the image
public:
    // Constructors taking an image as parameter
    SegmentedImage(i3d::Image3d<i3d::GRAY8> &image);
    SegmentedImage(i3d::Image3d<i3d::RGB> &image);
    // Constructors taking an image and an existing segmentation as parameters
    SegmentedImage(i3d::Image3d<i3d::GRAY8> &image, i3d::Image3d<i3d::GRAY16> &segmentation);
    SegmentedImage(i3d::Image3d<i3d::RGB> &image, i3d::Image3d<i3d::GRAY16> &segmentation);
    
    // Adds labeled pixels from 'image' to '_labeledImage'.
    // Zero pixels are ignored.
    // Label 65535 in 'image' is used to force background and is replaced with 0 in '_labeledImage'.
    void AddLabels(i3d::Image3d<i3d::GRAY16> &image);
    
    // Adds one label to '_labeledImage'.
    // Each pixel that is Segmenter::BinaryLabelType::Foreground in 'image' is replaced by 'label' in '_labeledImage'.
    void AddBinaryLabel(i3d::Image3d<i3d::GRAY16> &image,  i3d::GRAY16 label);
    
    // Returns type of the segmented image
    i3d::ImgVoxelType GetType() {return _type;};
    // Used to conveniently check the type of the image.
    bool IsGray8() { return _type == i3d::Gray8Voxel;}
    bool IsRGB() { return _type == i3d::RGBVoxel;}
    
    // Used to get original type. If original image is of type i3d::Gray8Voxel, then it is converted to RGB.
    i3d::Image3d<i3d::RGB> GetOriginalImage();
    // Used to get original RGB image.
    i3d::Image3d<i3d::RGB> GetOriginalRgbImage();
    // Used to get original grayscale image.
    i3d::Image3d<i3d::GRAY8> GetOriginalGrayImage();
    // Used to get labeled image.
    i3d::Image3d<i3d::GRAY16> GetLabeledImage();
    
    // Methods used to get bitmaps for drawing on screen.
    wxBitmap GetOriginalBitmap();
    wxBitmap GetLabeledBitmap();
    wxBitmap GetLabeledOutlinedBitmap();
    
    // Used to get dimensions of the image.
    size_t GetWidth() { return _width; }
    size_t GetHeight() { return _height; }
    
    // Used to get a label that has not been used before in _labeledImage
    i3d::GRAY16 GetMaxUsedLabel() {return _labeledImage.GetMaxValue();}
    
    // Used to repaint component on coordinates ['x', 'y'] with 'label'.
    void RepaintComponent(int x, int y, i3d::GRAY16 label);
    // Used to repaint all components that have the same label as component on coordinates ['x', 'y'].
    // Components are repainted with 'label'.
    void RepaintAllComponents(int x, int y, i3d::GRAY16 label);
    
    ~SegmentedImage();
    
protected:
    // Updates '_labeledBitmap' with contents of '_labeledImage'.
    void UpdateLabeledBitmap();
    // Updates '_labeledOutlineBitmap' with outlined contents of '_labeledImage'.
    void UpdateLabeledOutlineBitmap();
    // Computes outlined bitmap from contents of 'image'.
    void ComputeLabeledOutlineBitmap(i3d::Image3d<i3d::GRAY16> &image);
};


#endif /* SegmentedImage_hpp */
