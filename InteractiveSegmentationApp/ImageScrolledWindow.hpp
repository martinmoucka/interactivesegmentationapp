//
//  ImagePanel.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 04.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef ImageScrolledWindow_hpp
#define ImageScrolledWindow_hpp

#include <stdio.h>
#include <wx/wx.h>
//#include "MainFrame.h"

class MainFrame;

class ImageScrolledWindow : public wxScrolledWindow
{
    size_t _zoom = 10000; // Current zoom. 10000 = 100%
    wxSize _contentSize; // Size of image inside the window.
    MainFrame* _parentFrame; // Parent frame.
public:
    // Constructor
    ImageScrolledWindow(wxWindow *parent,
                        wxWindowID winid = wxID_ANY,
                        const wxPoint& pos = wxDefaultPosition,
                        const wxSize& size = wxDefaultSize,
                        long style = wxScrolledWindowStyle,
                        const wxString& name = wxPanelNameStr);
    
    // Used to set parent frame.
    void SetParentFrame(MainFrame* parent) { _parentFrame = parent; }
    // Used to set size of image inside the window.
    void SetContentSize(const wxSize& size);
    // Used to set zoom.
    void SetZoom(size_t _zoom);
    // Used to get zoom.
    size_t GetZoom() const { return _zoom; }
    // Used to get zoom scale. 100% = 1, 50% = 0.5, ...
    double GetZoomScale() {return (double)(_zoom)/10000.0;}
    
    // Events handling
    // ---------------
    // Used to handle paint event.
    void OnPaint(wxPaintEvent & evt);
    // Used to handle size event.
    void OnSize(wxSizeEvent& event);
    
    // Mouse events
    // Methods to handle various mouse events.
    virtual void OnMouseLeftDown(wxMouseEvent& event);
    virtual void OnMouseLeftUp(wxMouseEvent& event);
    virtual void OnMouseMotion(wxMouseEvent& event);
    virtual void OnMouseRightDown(wxMouseEvent& event);
    virtual void OnMouseLeaveWindow(wxMouseEvent& event);
    // Used to adjust event coordinates according to the scroll position and zoom so that
    // returned event's position is in image coordinates.
    wxMouseEvent AdjustEventCoordinates(wxMouseEvent& event);
    // Used to check whether the point is inside the image.
    bool PointIsInsideImage(const wxPoint& point);
    
    // Used to render contents. Here we only prepare the dc.
    // Actual rendering is done in Segmenter::Render(wxDC& dc).
    void Render(wxDC& dc);
    
    
private:
    // Used to set scrollbars according to image size.
    void SetScrollbars();
    
    DECLARE_EVENT_TABLE()
};
#endif /* ImagePanel_hpp */
