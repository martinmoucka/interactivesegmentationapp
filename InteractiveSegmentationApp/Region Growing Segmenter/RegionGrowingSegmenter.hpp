//
//  RegionGrowingSegmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 06.05.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef RegionGrowingSegmenter_hpp
#define RegionGrowingSegmenter_hpp

#include <stdio.h>
#include "NaryScribbleSegmenter.hpp"
#include "RegionGrowingSegmenterBackend.hpp"

// Segmenter implementing Simple Region Growing

class RegionGrowingSegmenter : public NaryScribbleSegmenter {
    
    // Tools: 'Wall' and 'Seed'
    // -----------------------
    enum Tool {Wall, Seed};
    // Variable keeping track of the active tool
    Tool _tool = Seed;
    // Used to set the active tool.
    // If 'tool' == Wall, _drawingLabel is set to Background
    // If 'tool' == Seed, _drawingLabel is set to Foreground
    void SetTool(Tool tool);
    
    // Segmenter backends that actually performs computation.
    RegionGrowingSegmenterBackend<i3d::RGB> _backendRGB;
    RegionGrowingSegmenterBackend<i3d::GRAY8> _backendGray;
    
    // Internal variables
    wxPoint _seedCenter; // Used to hold position of the active seed point.
    double _h; // Current 'h' parameter, that changes when user drags the mouse.
    bool _drawH = false; // Should 'h' be drawn above the mouse cursor?
    
public:
    // Constructor
    RegionGrowingSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    // Init method
    virtual void Init();
    
    // Overriding methods
    void SetImage(std::shared_ptr<SegmentedImage> image, std::string name);
    virtual void AddButtonsToRibbon();
     virtual void OnRibbonButtonClicked(wxRibbonButtonBarEvent& event);
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask);
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask);
    virtual void AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels);
    
    // Mouse events handling
    virtual void OnMouseLeftDown(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseLeftUp(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseMotion(wxMouseEvent& event, bool insideImage);
    
    virtual void UpdateUI();
    virtual void Render(wxDC& dc);
    
    // Used to draw 'h' above the mouse cursor
    virtual void DrawH(wxDC& dc);
    
    // Definiton of ids for tool buttons
    enum {
        ID_BUTTON_WALL = 100001,
        ID_BUTTON_SEED = 100002,
    };
};



#endif /* RegionGrowingSegmenter_hpp */
