//
//  RegionGrowingSegmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 06.05.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "RegionGrowingSegmenter.hpp"
#include "RegionGrowingSegmenterBackend.hpp"

RegionGrowingSegmenter::RegionGrowingSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame) : NaryScribbleSegmenter(id, ribbonBar, parentFrame) {
    _name = "Region Growing Segmenter";
    _type = Binary;
    _showBoundingBoxButton = false;
    _showRunSegmentationButton = false;
    _showClearSegmentationResultButton = false;
    _showAddToSegmentationButton = false;
    _showClearMarkersButton = false;
}

void RegionGrowingSegmenter::Init() {
    NaryScribbleSegmenter::Init();
    _width = 2;
    _tool = Wall;
    UpdateUI();
}

void RegionGrowingSegmenter::SetImage(std::shared_ptr<SegmentedImage> image, std::string name) {
    NaryScribbleSegmenter::SetImage(image, name);
    if (image->IsGray8()) {
        _backendGray.SetImage(image->GetOriginalGrayImage());
    } else {
        _backendRGB.SetImage(image->GetOriginalRgbImage());
    }
}

void RegionGrowingSegmenter::SetTool(RegionGrowingSegmenter::Tool tool){
    _tool = tool;
    if (_tool == Wall) {
        _drawingLabel = Segmenter::BinaryLabelType::Background;
    } else {
        _drawingLabel = Segmenter::BinaryLabelType::Foreground;
    }
    UpdateUI();
}

void RegionGrowingSegmenter::AddButtonsToRibbon() {
    
    NaryScribbleSegmenter::AddButtonsToRibbon();
    
    _ribbonButtonBar->AddButton(ID_BUTTON_WALL, _("Wall"), wxArtProvider::GetBitmap(wxART_TIP, wxART_BUTTON, wxDefaultSize), _("Draw wall"), wxRIBBON_BUTTON_TOGGLE);
    
    _ribbonButtonBar->AddButton(ID_BUTTON_SEED, _("Seed"), wxArtProvider::GetBitmap(wxART_TIP, wxART_BUTTON, wxDefaultSize), _("Place seed"), wxRIBBON_BUTTON_TOGGLE);
    
}

// MARK: - Mouse event handling

void RegionGrowingSegmenter::OnMouseLeftDown(wxMouseEvent &event, bool insideImage)
{
    NaryScribbleSegmenter::OnMouseLeftDown(event, insideImage);
    if (_tool == Seed) {
        NaryScribbleSegmenter::OnMouseLeftDown(event, insideImage);
        auto mask =  Segmenter::ConvertLabelWxImageToImage3dGRAY16(_markersBitmap.ConvertToImage());
        if (_segmentedImage->IsGray8()) {
            _backendGray.SetMask(mask);
        } else {
            _backendRGB.SetMask(mask);
        }
        _seedCenter = event.GetPosition();
        _drawH = true;
    }
    
}

void RegionGrowingSegmenter::OnMouseLeftUp(wxMouseEvent &event, bool insideImage)
{
    _drawH = false;
    if (_tool == Seed) {
        AddSegmentationResultToSegmentedImageWrapper();
    }
    NaryScribbleSegmenter::OnMouseLeftUp(event, insideImage);
    
}

void RegionGrowingSegmenter::OnMouseMotion(wxMouseEvent &event, bool insideImage)
{
    if (_tool == Wall) {
        NaryScribbleSegmenter::OnMouseMotion(event, insideImage);
    } else {
        _cursorPosition = event.GetPosition();
        if (!event.LeftIsDown()){
            RefreshCanvas();
            return;
        }
        double diffx = _seedCenter.x - event.GetPosition().x;
        double diffy = _seedCenter.y - event.GetPosition().y;
        _h = std::sqrt( diffx*diffx + diffy*diffy );
        OnRunSegmentation();

    }
}

i3d::Image3d<i3d::GRAY16> RegionGrowingSegmenter::RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask) {
    return _backendGray.RunSegmentation(_seedCenter.x, _seedCenter.y, _h);;
}

i3d::Image3d<i3d::GRAY16> RegionGrowingSegmenter::RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask) {
    return _backendRGB.RunSegmentation(_seedCenter.x, _seedCenter.y, _h);;
}

void RegionGrowingSegmenter::AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels) {
    _segmentedImage->AddBinaryLabel(labels, _currentLabel);
}

void RegionGrowingSegmenter::OnRibbonButtonClicked(wxRibbonButtonBarEvent &event)
{
    int id = _ribbonButtonBar->GetItemId(event.GetButton());
    switch (id) {
        case ID_BUTTON_WALL:
            if (_tool == Wall) {
                SetTool(Seed);
            } else {
                SetTool(Wall);
            }
            break;
        case ID_BUTTON_SEED:
            if (_tool == Seed) {
                SetTool(Wall);
            } else {
                SetTool(Seed);
            }
            break;
        default:
            NaryScribbleSegmenter::OnRibbonButtonClicked(event);
            break;
    }
}

void RegionGrowingSegmenter::Render(wxDC &dc) {
    NaryScribbleSegmenter::Render(dc);
    if (_drawH) DrawH(dc);
    if (_tool == Seed) DrawCursor(dc);
}

void RegionGrowingSegmenter::DrawH(wxDC &dc) {
    wxGraphicsContext *gc = wxGraphicsContext::CreateFromUnknownDC(dc);
    gc->SetPen(wxPen(wxColour(255, 255, 160, 255), 3, wxPENSTYLE_SOLID));
    gc->SetBrush(wxBrush(wxColour(0, 0, 0, 0)));
    wxFont font;
    font.SetPointSize(12);
    font.SetWeight(wxFONTWEIGHT_NORMAL);
    gc->SetFont(font, wxColour(255, 255, 160, 255));
    gc->DrawText(wxString::Format(wxT("%f"),_h), _cursorPosition.x + 10, _cursorPosition.y - 10);
    gc->DrawRectangle(_bbox.GetX(), _bbox.GetY(), _bbox.GetWidth(), _bbox.GetHeight());
    delete gc;
}

void RegionGrowingSegmenter::UpdateUI() {
    NaryScribbleSegmenter::UpdateUI();
    _ribbonButtonBar->ToggleButton(ID_BUTTON_WALL, _tool == Wall);
    _ribbonButtonBar->ToggleButton(ID_BUTTON_SEED, _tool == Seed);
}
