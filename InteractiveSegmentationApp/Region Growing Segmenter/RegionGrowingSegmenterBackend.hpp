//
//  RegionGrowingSegmenterBackend.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 06.05.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef RegionGrowingSegmenterBackend_hpp
#define RegionGrowingSegmenterBackend_hpp

#include <stdio.h>
#include <i3d/image3d.h>
#include <i3d/draw.h>
#include "Segmenter.hpp"
#include "Util.hpp"

// Class implementing the computation part of the Simple Region Growing technique
// It should be used followingly:
// 1. Construct RegionGrowingSegmenterBackend()
// 2. Set image by calling SetImage(...)
// 3. Set mask, where seed points are indicated with Segmenter::BinaryLabelType::Foreground
//    by calling SetMask(...)
// 4. Iteratively call RunSegmentation()

template <class PIXEL> class RegionGrowingSegmenterBackend{
    i3d::Image3d<PIXEL> _image;
    i3d::Image3d<i3d::GRAY16> _mask;
    PIXEL _avgValue;
    i3d::Image3d<i3d::GRAY16> _diffMap;

public:
    // Constructor
    RegionGrowingSegmenterBackend() {}
    
    // Used to set image
    void SetImage(i3d::Image3d<PIXEL> image) {
        _image = image;
    }
    
    // Used to set mask, where seed pixels should have value of Segmenter::BinaryLabelType::Foreground
    void SetMask(i3d::Image3d<i3d::GRAY16> mask) {
        _mask = mask;
        ComputeSeedAverageValue(_image);
        ComputeDifferenceMap();
    }
    
    // Computes the segmentation result
    // 'x' and 'y' is the center of the seed area. 'h' is parameter h.
    i3d::Image3d<i3d::GRAY16> RunSegmentation(size_t x, size_t y, size_t h) {
        i3d::Image3d<i3d::GRAY16> thresholded, filled;
        thresholded.MakeRoom(_diffMap.GetWidth(), _diffMap.GetHeight(), 1);
        filled.MakeRoom(_diffMap.GetWidth(), _diffMap.GetHeight(), 1);
        
        // Thresholding
        for(size_t i = 0; i < _diffMap.GetImageSize(); ++i) {
            if( _diffMap.GetVoxel(i) <= h ) {
                thresholded.SetVoxel(i, BinaryLabelType::Foreground);
            } else {
                thresholded.SetVoxel(i, BinaryLabelType::Background);
            }
        }
        thresholded.SetVoxel(x, y, 0, BinaryLabelType::Foreground);
        i3d::FloodFill(thresholded, filled, (i3d::GRAY16)(BinaryLabelType::Foreground), x, y, 0);
        return filled;
    }
    
private:
    
    void ComputeDifferenceMap() {
        _diffMap.MakeRoom(_image.GetWidth(), _image.GetHeight(), 1);
        for (size_t i = 0; i < _image.GetImageSize(); ++i) {
            if (_mask.GetVoxel(i) == Segmenter::BinaryLabelType::Background) {
                _diffMap.SetVoxel(i, 65535);
            } else {
                double dist = IntensityDistance(_image.GetVoxel(i), _avgValue);
                _diffMap.SetVoxel(i, dist);
            }
        }
    }
    
    // Used to compute average value of the seed region in 8bit grayscale image
    void ComputeSeedAverageValue(i3d::Image3d<i3d::GRAY8> &image) {
        size_t avg = 0, count = 0;
        for (size_t i = 0; i < image.GetImageSize(); ++i) {
            if (_mask.GetVoxel(i) == Segmenter::BinaryLabelType::Foreground) {
                avg += image.GetVoxel(i);
                ++count;
            }
        }
        _avgValue = double(avg)/double(count);
    }
    
    // Used to compute average value of the seed region in RGB image
    void ComputeSeedAverageValue(i3d::Image3d<i3d::RGB> &image) {
        double avgR = 0, avgG = 0, avgB = 0, count = 0;
        for (size_t i = 0; i < image.GetImageSize(); ++i) {
            if (_mask.GetVoxel(i) == Segmenter::BinaryLabelType::Foreground) {
                avgR += image.GetVoxel(i).red;
                avgG += image.GetVoxel(i).green;
                avgB += image.GetVoxel(i).blue;
                ++count;
            }
        }
        avgR = avgR/double(count);
        avgG = avgG/double(count);
        avgB = avgB/double(count);
        _avgValue = i3d::RGB_generic<i3d::GRAY8>(avgR, avgG, avgB);
        std::cout << "AVG val: " << avgR << " " << avgG << " " << avgB << std::endl;
    }
};
#endif /* RegionGrowingSegmenterBackend_hpp */
