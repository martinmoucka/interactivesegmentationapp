//
//  Util.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 09.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "Util.hpp"

double IntensityDistance(i3d::GRAY8 a, i3d::GRAY8 b){
    return (double)(std::abs( a - b ));
}

double IntensityDistance(i3d::RGB aa, i3d::RGB bb) {
    double dr = aa.red - bb.red;
    double dg = aa.green - bb.green;
    double db = aa.blue - bb.blue;
    double sum = dr*dr + dg*dg + db*db;
    return std::sqrt(sum);
}

double IntensityDistance(i3d::RGB_generic<double> a, i3d::RGB_generic<double> b) {
    i3d::RGB_generic<double> diff = a - b;
    diff.red = diff.red*diff.red;
    diff.green = diff.green*diff.green;
    diff.blue = diff.blue*diff.blue;
    double sum = diff.red + diff.green + diff.blue;
    return std::sqrt(sum);
}

double IntensityDistance(i3d::RGB_generic<double> a, i3d::RGB b) {
    i3d::RGB_generic<double> bd(b.red, b.green, b.blue);
    return IntensityDistance(a, bd);
}

wxRect FixRect(wxRect rect) {
    if (rect.GetHeight() == 0 &&rect.GetWidth() == 0) return rect;
    int top = rect.GetTop();
    int bottom = rect.GetBottom();
    int left = rect.GetLeft();
    int right = rect.GetRight();
    return wxRect(
                  wxPoint(std::min(left, right), std::min(top, bottom)),
                  wxPoint(std::max(left, right), std::max(top, bottom))
                  );
}

wxBitmap CreateLabelButtonIcon(unsigned int label, wxColor color) {
    wxBitmap bmp(32, 32);
    wxMemoryDC dc(bmp);
    dc.SetBrush(wxBrush(color,wxBRUSHSTYLE_SOLID) );
    dc.DrawRectangle(0, 0, 32, 32);
    dc.SetPen( wxPen(wxColor(255,255,255), 2, wxPENSTYLE_SOLID) );
    wxFont font;
    font.SetPointSize(16);
    font.SetWeight(wxFONTWEIGHT_BOLD);
    dc.SetFont( font );
    
    dc.SetTextForeground(wxColor(0,0,0));
    dc.DrawLabel(wxString::Format(wxT("%i"),label), wxNullBitmap, wxRect(3, 3, 24, 24), wxALIGN_CENTER);
    dc.DrawLabel(wxString::Format(wxT("%i"),label), wxNullBitmap, wxRect(3, 5, 24, 24), wxALIGN_CENTER);
    dc.DrawLabel(wxString::Format(wxT("%i"),label), wxNullBitmap, wxRect(5, 3, 24, 24), wxALIGN_CENTER);
    dc.DrawLabel(wxString::Format(wxT("%i"),label), wxNullBitmap, wxRect(5, 5, 24, 24), wxALIGN_CENTER);
    dc.SetTextForeground(wxColor(255,255,255));
    //dc.DrawText( wxString::Format(wxT("%i"),label), 4, 4);
    dc.DrawLabel(wxString::Format(wxT("%i"),label), wxNullBitmap, wxRect(4, 4, 24, 24), wxALIGN_CENTER);
    return bmp;
}
