#ifndef MAINFRAME_H
#define MAINFRAME_H
#include <wx/dir.h>
#include <i3d/image3d.h>
#include "InteractiveSegmentationApp_ui.h"
#include "Segmenter.hpp"
#include "BinaryScribbleSegmenter.hpp"
#include "NaryScribbleSegmenter.hpp"
#include "GraphCutSegmenter.hpp"
#include "RandomWalkSegmenter.hpp"
#include "GrabCutSegmenter.hpp"
#include "RegionGrowingSegmenter.hpp"
#include "ManualSegmenter.hpp"
#include <vector>

// Main component of the application

class MainFrame : public MainFrameBase
{
    wxArrayString _imagePaths; // Stores paths to currently opened images.
    std::vector< std::shared_ptr<SegmentedImage> > _segmentedImages;
    size_t _imageIndex = 0; // Index of the current image.
    size_t _zoom = 10000; // Current zoom of the image in percent. 10000 == 100%
    std::vector< std::shared_ptr<Segmenter> > _segmenters; // Segmenters
    std::shared_ptr<Segmenter> _activeSegmenter; // Pointer to active segmenter.
    
    
public:
    // Constructor
    MainFrame(wxWindow* parent);
    // Destructor
    virtual ~MainFrame();
    // Used to set active segmenter with 'id'. To get id of segmenter, you should use Segmenter::GetId().
    void SetActiveSegmenter(int id);
    // Used to get pointer to active segmenter.
    std::shared_ptr<Segmenter> GetActiveSegmenter();
    
    // Handling of events that come from segmenters
    // ---------------
    // Called when 'Next Image' button is clicked.
    virtual void OnNextImage(wxCommandEvent& event);
    // Called when 'Previous Image' button is clicked.
    virtual void OnPreviousImage(wxCommandEvent& event);
    // Called when 'Zoom In' button is clicked.
    virtual void OnZoomIn(wxCommandEvent& event);
    // Called when 'Zoom Out' button is clicked.
    virtual void OnZoomOut(wxCommandEvent& event);
    // Used to set unused label to all segmenters.
    // Called when 'Set unused label' menu item is selected or 'U' key is pressed.
    void SetUnusedLabelToAllSegmenters();
    // Used to set previous label to all segmenters.
    // Called when 'Previous label' is clicked or 'F1' key is pressed.
    void SetPreviousLabelToAllSegmenters();
    // Used to set next label to all segmenters.
    // Called when 'Next label' is clicked or 'F2' key is pressed.
    void SetNextLabelToAllSegmenters();
    // Used to set 'label' label to all segmenters
    void SetLabelToAllSegmenters(i3d::GRAY16 label);
    
protected:
    
    // Handling of menu events
    // -----------------------
    // Called when 'Save all segmentations' menu item is selected.
    virtual void OnSaveAllSegmentations(wxCommandEvent& event);
    // Called when 'Save segmentation' menu item is selected.
    virtual void OnSaveSegmentation(wxCommandEvent& event);
    // Called when 'Open directory...' menu item is selected.
    virtual void OnOpenDirectory(wxCommandEvent& event);
    // Called when 'Open Files...' menu item is selected.
    virtual void OnOpenFiles(wxCommandEvent& event);
    
    // Saving files
    // ------------
    // Used to get path of temporary segmentation file.
    // This file is located in 'tmp' directory that is at the same location as original image.
    // Filename is equal to the name of original image. Extension is '.png'
    wxFileName TemporarySegmentationFilePath(wxString& imagePath);
    // Used to get path of temporary segmentation file.
    // This file is located in 'segmented' directory that is at the same location as original image.
    // Filename is equal to the name of original image. Extension is '.png'
    wxFileName SegmentationFilePath(wxString& imagePath);
    // Used to save temporary segmentation of current image.
    // It is used when switching images so that all images are not loaded in memory.
    virtual void SaveCurrentTemporarySegmentation();
    // Used to save the segmentation file of current image.
    virtual void SaveCurrentSegmentation();
    // Used to save labeled image 'segmentation' to path 'path'.
    virtual void SaveSegmentationFile(i3d::Image3d<i3d::GRAY16>& segmentation, wxFileName& path);
    // Deletes temporary segmentation files of each opened image.
    virtual void DeleteTemporaryFiles();
    
    // Handling of events
    // ------------------
    // Called when ribbon page is changed to set the corresponding segmenter as active.
    virtual void OnRibbonBarPageChanged(wxRibbonBarEvent& event);
    // Handles key press.
    virtual void OnKeyDown(wxKeyEvent& event);
    // Called when 'Repaint this component...' is selected from menu after right click on image.
    virtual void OnRightMenuRepaintComponent(wxCommandEvent& event);
    // Called when 'Repaint all components...' is selected from menu after right click on image.
    virtual void OnRightMenuRepaintAllComponents(wxCommandEvent& event);
    // Called when 'Select this label' is selected from menu after right click on image.
    virtual void OnRightMenuSelectLabel(wxCommandEvent& event);
    // Handles exit event
    virtual void OnExit(wxCommandEvent& event);
    
    // Sets active image as image with index 'index'
    void SetImageIndex(size_t index, bool saveTemporarySegmentation = true);
    
    // Used to refresh ImageScrolledWindow
    void Update();
};
#endif // MAINFRAME_H
