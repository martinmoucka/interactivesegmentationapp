//
//  ScribbleSegmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 21.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef ScribbleSegmenter_hpp
#define ScribbleSegmenter_hpp

#include <stdio.h>
#include <wx/dcmemory.h>
#include <wx/graphics.h>
#include "Segmenter.hpp"

// Specialization of SegmenterMouseMode class
class ScribbleSegmenterMouseMode : public SegmenterMouseMode {
    bool _scribble;
protected:
    virtual void UnsetAll() {_scribble = false; SegmenterMouseMode::UnsetAll();}
public:
    void SetScribble() { UnsetAll(); _scribble = true;}
    bool IsScribble() {return _scribble;}
};

// Specialization of Segmenter class.
// This class adds functionality for drawing scribbles.
class ScribbleSegmenter: public Segmenter {
    
protected:
    virtual ScribbleSegmenterMouseMode* GetMouseMode() {return (ScribbleSegmenterMouseMode*)_mouseMode;}
    i3d::GRAY16 _maxLabel = 1;
    
   // virtual void AddRibbonPage();
    virtual void AddButtonsToRibbon();
    
    // internal state for e.g. drawing
    i3d::GRAY16 _drawingLabel;
    unsigned int _width = 3;
    wxPoint _lastPoint;
    bool _drawCursor = false;
    wxPoint _cursorPosition;

public:
    ScribbleSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    
    // Override methods
    virtual void SetImage(std::shared_ptr<SegmentedImage> image, std::string name);
    virtual void SetLabel(i3d::GRAY16 label, wxBitmap bmp);
    
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask);
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask);
   
protected:
    // Drawing
    virtual void Render(wxDC& dc);
    // Used to draw mouse cursor.
    virtual void DrawCursor(wxDC& dc);
    // Used to draw scribble line.
    virtual void DrawLine(wxPoint a, wxPoint b);
    // Used to draw single point.
    virtual void DrawPoint(wxPoint p);
    
    // Mouse events handling
    virtual void OnMouseLeftDown(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseLeftUp(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseMotion(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseLeave(wxMouseEvent& event);
    virtual void OnKeyDown(wxKeyEvent& event);
    
    // Event handling
    virtual void OnRibbonButtonClicked(wxRibbonButtonBarEvent& event);
    virtual void ToggleBoundingBox();
    // Used to increase brush size.
    void IncreaseBrushSize();
    // Used to decrease brush size.
    void DecreaseBrushSize();
    
    // Define new ids for buttons
    enum {
        ID_BUTTON_BRUSH_SIZE_INCREASE = 1001,
        ID_BUTTON_BRUSH_SIZE_DECREASE = 1002,
    };
};

#endif /* ScribbleSegmenter_hpp */
