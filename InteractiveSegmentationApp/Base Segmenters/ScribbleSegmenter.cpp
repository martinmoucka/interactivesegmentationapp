//
//  ScribbleSegmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 21.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "ScribbleSegmenter.hpp"
#include <wx/rawbmp.h>
#include "MainFrame.h"

ScribbleSegmenter::ScribbleSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame):
Segmenter(id, ribbonBar, parentFrame) {
    _name = "ScribbleSegmenter";
    if (_mouseMode != NULL) delete _mouseMode;
    _mouseMode = new ScribbleSegmenterMouseMode();
    GetMouseMode()->SetScribble();
}


// MARK: - User interface

void ScribbleSegmenter::AddButtonsToRibbon() {
    if (!_ribbonButtonBar) {
        std::cerr << "AddButtonsToRibbon called on Segmenter with no _ribbonButtonBar" << std::endl;
        return;
    }
    
    _ribbonButtonBar->AddButton(ID_BUTTON_BRUSH_SIZE_DECREASE, _("Brush size (F3)"), wxArtProvider::GetBitmap(wxART_MINUS, wxART_BUTTON, wxDefaultSize), _("Decrease brush size"), wxRIBBON_BUTTON_NORMAL);
    
    _ribbonButtonBar->AddButton(ID_BUTTON_BRUSH_SIZE_INCREASE, _("Brush size (F4)"), wxArtProvider::GetBitmap(wxART_PLUS, wxART_BUTTON, wxDefaultSize), _("Increase brush size"), wxRIBBON_BUTTON_NORMAL);
    
    Segmenter::AddButtonsToRibbon();
}

// MARK: - Image handling

void ScribbleSegmenter::SetImage(std::shared_ptr<SegmentedImage> image,  std::string name) {
    Segmenter::SetImage(image, name);
    _maxLabel = 1;
    ResetUI();
}

void ScribbleSegmenter::SetLabel(i3d::GRAY16 label, wxBitmap bmp) {
    Segmenter::SetLabel(label, bmp);
    if (_type == Nary) {
        _drawingLabel = label;
    }
}

i3d::Image3d<i3d::GRAY16> ScribbleSegmenter::RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask) {
    return mask;
}

i3d::Image3d<i3d::GRAY16> ScribbleSegmenter::RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask) {
    return mask;
}

// MARK: - Drawing

void ScribbleSegmenter::DrawCursor(wxDC& dc) {
    wxGraphicsContext *gc = wxGraphicsContext::CreateFromUnknownDC(dc);
    gc->SetPen(wxPen( RGBToWxColour( ColorFromLabel(_drawingLabel) ), 1, wxPENSTYLE_SOLID));
    gc->SetBrush(wxBrush(wxColour(0, 0, 0, 0)));
    double halfWidth = (double)_width/2.0;
    gc->DrawEllipse(_cursorPosition.x - halfWidth, _cursorPosition.y - halfWidth, _width, _width);
    delete gc;
}

void ScribbleSegmenter::DrawLine(wxPoint a, wxPoint b) {

    // Draw to label bitmap
    i3d::GRAY16 label = _drawingLabel;
    if (label == 0) label = std::numeric_limits<i3d::GRAY16>::max();
    wxGraphicsContext *lgc = wxGraphicsContext::Create(_markersBitmap);
    lgc->SetAntialiasMode(wxANTIALIAS_NONE);
    lgc->SetPen( wxPen( Segmenter::ConvertGRAY16LabelToInternalwxColor(label), _width, wxPENSTYLE_SOLID) );
	lgc->StrokeLine(a.x, a.y, b.x, b.y);
    delete lgc;
    
    // Draw to bitmap that is displayed
    wxGraphicsContext *dgc = wxGraphicsContext::Create(_markersDisplayBitmap);
    dgc->SetAntialiasMode(wxANTIALIAS_NONE);
    wxColour color = RGBToWxColour(ColorFromLabel(_drawingLabel));
    dgc->SetPen( wxPen( color , _width, wxPENSTYLE_SOLID) );
	dgc->StrokeLine(a.x, a.y, b.x, b.y);
    delete dgc;
}

void ScribbleSegmenter::DrawPoint(wxPoint p) {
    
    double halfWidth = (double)_width/2.0;
    
    // Draw to label bitmap
    i3d::GRAY16 label = _drawingLabel;
    if (label == 0) label = std::numeric_limits<i3d::GRAY16>::max();
    wxGraphicsContext *lgc = wxGraphicsContext::Create(_markersBitmap);
    lgc->SetAntialiasMode(wxANTIALIAS_NONE);
    wxColour colorL = Segmenter::ConvertGRAY16LabelToInternalwxColor(label);
    //lgc->SetPen( wxPen(colorL, _width, wxPENSTYLE_SOLID) );
    lgc->SetBrush(wxBrush(colorL));
    lgc->DrawEllipse(p.x - halfWidth, p.y - halfWidth, _width, _width);
    delete lgc;
    
    // Draw to bitmap that is displayed
    wxGraphicsContext *dgc = wxGraphicsContext::Create(_markersDisplayBitmap);
    dgc->SetAntialiasMode(wxANTIALIAS_NONE);
    wxColour colorD = RGBToWxColour(ColorFromLabel(_drawingLabel));
    //dgc->SetPen( wxPen( colorD , _width, wxPENSTYLE_SOLID) );
    dgc->SetBrush(wxBrush(colorD));
    dgc->DrawEllipse(p.x - halfWidth, p.y - halfWidth, _width, _width);
    delete dgc;
}

void ScribbleSegmenter::Render(wxDC &dc) {
    Segmenter::Render(dc);
    if (GetMouseMode()->IsScribble() && _drawCursor) DrawCursor(dc);
}

    
// MARK: - Mouse event handling

void ScribbleSegmenter::OnMouseLeftDown(wxMouseEvent &event, bool insideImage)
{
    if (GetMouseMode()->IsScribble()) {
        _lastPoint = event.GetPosition();
        DrawPoint(_lastPoint);
    }
    Segmenter::OnMouseLeftDown(event, insideImage);
}

void ScribbleSegmenter::OnMouseLeftUp(wxMouseEvent &event, bool insideImage){
    Segmenter::OnMouseLeftUp(event, insideImage);
}

void ScribbleSegmenter::OnMouseMotion(wxMouseEvent &event, bool insideImage)
{
    if (GetMouseMode()->IsScribble()) {
        if (event.LeftIsDown()) {
            // Draw to label bitmap
            DrawLine(_lastPoint, event.GetPosition());
            
            _lastPoint = event.GetPosition();
            _maxLabel = std::max(_maxLabel, _drawingLabel);
        }
        _drawCursor = true;
        _cursorPosition = event.GetPosition();
    }
    
    Segmenter::OnMouseMotion(event, insideImage);
}

void ScribbleSegmenter::OnMouseLeave(wxMouseEvent& event) {
    _drawCursor = false;
    Segmenter::OnMouseLeave(event);
}

void ScribbleSegmenter::OnKeyDown(wxKeyEvent &event) {
    switch (event.GetKeyCode()) {
            
        case WXK_F3:
            DecreaseBrushSize();
            break;
        case WXK_F4:
            IncreaseBrushSize();
            break;
        default:
            Segmenter::OnKeyDown(event);
            break;
    }
}

// MARK: - Event handling

void ScribbleSegmenter::OnRibbonButtonClicked(wxRibbonButtonBarEvent &event)
{
    int id = _ribbonButtonBar->GetItemId(event.GetButton());
    switch (id) {
        case ID_BUTTON_BRUSH_SIZE_INCREASE:
            IncreaseBrushSize();
            break;
        case ID_BUTTON_BRUSH_SIZE_DECREASE:
            DecreaseBrushSize();
            break;
        default:
            Segmenter::OnRibbonButtonClicked(event);
        break;
    }
}

void ScribbleSegmenter::IncreaseBrushSize() {
    printf("Increase brush\n");
    _width = _width + 1;
    printf("Brush size: %d\n", _width);
     RefreshCanvas();
}

void ScribbleSegmenter::DecreaseBrushSize() {
    printf("Decrease brush\n");
    if (_width > 1) {
        _width = _width - 1;
    }
    printf("Brush size: %d\n", _width);
     RefreshCanvas();
}

void ScribbleSegmenter::ToggleBoundingBox() {
    if (GetMouseMode()->IsBoundingBox()) {
        GetMouseMode()->SetScribble();
    } else {
        GetMouseMode()->SetBoundingBox();
    }
    UpdateUI();
}
