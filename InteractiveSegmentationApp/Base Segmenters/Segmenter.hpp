//
//  Segmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 08.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef Segmenter_hpp
#define Segmenter_hpp

#include <stdio.h>
#include <string>
#include <memory>
#include <wx/ribbon/bar.h>
#include <wx/ribbon/buttonbar.h>
#include <wx/ribbon/art.h>
#include <wx/ribbon/page.h>
#include <wx/ribbon/panel.h>
#include <wx/dcgraph.h>
#include <wx/xrc/xmlres.h>
#include <i3d/image3d.h>
#include <i3d/voi.h>
#include "SegmentedImage.hpp"

// Class to keep information about the mouse mode being used.
class SegmenterMouseMode {
    bool _boundingBox;
    bool _none;
protected:
    virtual void UnsetAll() {_boundingBox = false; _none = false;}
public:
    void SetBoundingBox() { UnsetAll(); _boundingBox = true;}
    void SetNone() { UnsetAll(); _none = true;}
    bool IsBoundingBox() {return _boundingBox;}
    bool IsNone() {return _none;}
};

// Forward declaration
class MainFrame;


// Base Segmenter class
class Segmenter {
    
public:
    // Enums
    enum SegmenterType { Binary, Nary };
    enum BinaryLabelType { Background = 1, Foreground = 2 };
    enum LabelsDisplayMode { Fill, Outline };
    enum {
        ID_BUTTON_COMMON_PREVIOUS_IMAGE = 10,
        ID_BUTTON_COMMON_NEXT_IMAGE = 11,
        ID_BUTTON_COMMON_ZOOM_OUT = 12,
        ID_BUTTON_COMMON_ZOOM_IN = 13,
        ID_BUTTON_COMMON_CURRENT_LABEL = 14,
        ID_BUTTON_COMMON_NEXT_UNUSED_LABEL = 15,
        ID_BUTTON_COMMON_BACKGROUND_LABEL = 16,
        ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS = 17,
        ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_FILL = 18,
        ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_OUTLINE = 19,
        ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_HIDE = 20,
        ID_RIGHT_CLICK_MENU_REPAINT_COMPONENT = 21,
        ID_RIGHT_CLICK_MENU_REPAINT_ALL_COMPONENTS = 22,
        ID_RIGHT_CLICK_MENU_SELECT_LABEL = 23,
        ID_BUTTON_RUN_SEGMENTATION = 101,
        ID_BUTTON_ADD_TO_SEGMENTED_IMAGE = 102,
        ID_BUTTON_CLEAR_ACTIVE_LABELS = 103,
        ID_BUTTON_CLEAR_SEGMENTATION_RESULT = 104,
        ID_BUTTON_BOUNDING_BOX = 105,
        ID_BUTTON_BOUNDING_BOX_RESET = 106,
    };
    
protected:
    
    //////////////////////
    // Internal variables
    //////////////////////
    
    // Id of segmenter
    int _id;
    // Name to display in the ribbon bar
    std::string _name = "Base Segmenter";
    // Type - either Binary or Nary
    SegmenterType _type;
    
    // Mouse mode of segmenter
    SegmenterMouseMode* _mouseMode;
    // You have to override this method and change the return type
    // accordingly, if you create a subclass of SegmenterMouseMode
    // for your new segmenter.
    virtual SegmenterMouseMode* GetMouseMode() {return _mouseMode;}
    
    // User interface  state variables
    // -------------------------------
    // Should the labels be displayed in Fill or Outline mode?
    LabelsDisplayMode _labelsDisplayMode = Fill;
    // Should the labels be displayed at all?
    bool _displayLabels = true;
    
    // Coordinates of last right click needs to be stored
    // for repainting the correct label after the menu is displayed after right click
    wxPoint _lastRightClick;
    
    // Current image being segmented
    std::shared_ptr<SegmentedImage> _segmentedImage;
    // Label that is used when adding regions to segmentation
    i3d::GRAY16 _currentLabel = 1;
    // Bounding Box
    wxRect _bbox = wxRect(0, 0, 0, 0);
    
    // Bitmaps used to store markers and intermediate
    // segmentation result internally and for display
    wxBitmap _markersBitmap;
    wxBitmap _markersDisplayBitmap;
    wxBitmap _segmentationResultBitmap;
    wxBitmap _segmentationResultDisplayBitmap;
    
    /////////////////
    // User interface
    /////////////////
    
    MainFrame* _parentFrame;
    wxRibbonBar* _parentRibbonBar;
    // Common ribbon bar.
    wxRibbonButtonBar* _commonRibbonButtonBar;
    // Ribbon bar you should ad new buttons to.
    wxRibbonButtonBar* _ribbonButtonBar;
    wxRibbonPage* _ribbonPage;
    
    // Switches for hiding/showing certain buttons in ribbonbar
    bool _showBoundingBoxButton = true;
    bool _showRunSegmentationButton = true;
    bool _showClearSegmentationResultButton = true;
    bool _showAddToSegmentationButton = true;
    bool _showClearMarkersButton = true;

    // Override this method to add Segmenter's ribbonPage to parent ribbonBar
    virtual void AddRibbonPage();
    
    // Adds ribbon panel common for all segmenters
    virtual void AddCommonRibbonPanel();
    // Used to add new buttons to ribbon var.
    // You should override this method and add new buttons in it.
    virtual void AddButtonsToRibbon();
    // Used to refresh ImageScrolledWindow.
    void RefreshCanvas();
    // Used to set buttons accordingly to the internal state
    virtual void UpdateUI();
    // Used to reset UI e.g. after new image is opened.
    virtual void ResetUI();
    // Used to handle events triggered by ribbon bar buttons.
    virtual void OnRibbonButtonClicked(wxRibbonButtonBarEvent& event);
    // Used to handle events triggered by ribbon dropdown buttons.
    virtual void OnRibbonButtonDropdownClicked(wxRibbonButtonBarEvent& event);
public:
    // Constructor
    Segmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    // Used to perform any custom initialization that needs to be done
    // after adding segmenter to ribbonbar.
    virtual void Init();
    int GetId() { return _id; }
    
    /////////////////
    // Image handling
    /////////////////
    // Used to set new SegmentedImage object that represents currently segmented image.
    virtual void SetImage(std::shared_ptr<SegmentedImage> image, std::string name);
    // Used to get SegmentedImage object.
    std::shared_ptr<SegmentedImage> GetSegmentedImage() {return _segmentedImage;}
    // Used to get segmentation result in form of labeled image.
    virtual i3d::Image3d<i3d::GRAY16> GetLabeledImage();
    // Used to get original image in form of wxBitmap.
    wxBitmap GetDisplayBitmap();
    // Used to set current label 'label'.
    // 'bmp' is bitmap displayed in 'Current Label' button.
    virtual void SetLabel(i3d::GRAY16 label, wxBitmap bmp);
    // Used to get current label.
    i3d::GRAY16 GetLabel() {return _currentLabel;}
    // Used to get maximum label that is currently present in segmentation result.
    i3d::GRAY16 GetMaxUsedLabel();
    // Used to get color for displaying it on screen from 'label'.
    virtual i3d::RGB ColorFromLabel(i3d::GRAY16 label);
   
    // Used to render contents to ImageScrolledWindow.
    virtual void Render(wxDC& dc);
    virtual void DrawBox(wxDC& dc);
    
    // Used to copmute segmentation of 'img'
    // 'mask' should contain user input.
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask) = 0;
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask) = 0;
    
    /////////////////
    // Event handling
    /////////////////
    
    // Called when 'Repaint this component...' is selected from the right-click menu.
    virtual void OnRepaintComponent();
    // Called when 'Repaint all components...' is selected from the right-click menu.
    virtual void OnRepaintAllComponents();
    // Called when 'Select this label' is selected from the right-click menu.
    virtual void OnSelectLabel();
    // Called when dropdown part of 'Current Label' button is clicked.
    virtual void OnCurrentLabelDropDown(wxRibbonButtonBarEvent& evt);
    // Called when dropdown part of 'Show Labels' button is clicked.
    virtual void OnShowLabelsDropDown(wxRibbonButtonBarEvent& evt);
    // Used to show right-click menu.
    virtual void ShowRightClickMenu();
    // Called after menu item is selected.
    virtual void OnMenuSelected(wxCommandEvent& event);
    // Called after 'Run Segmentation' button is clicked.
    virtual void OnRunSegmentation();
    // Called after 'Add to Segmentation' button is clicked.
    virtual void AddSegmentationResultToSegmentedImageWrapper();
    // Used to add intermediate segmentation result or other labeled image to final segmentation.
    virtual void AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels) = 0;
    // Used to remove bounding box. Called when 'Remove bounding box' is clicked.
    virtual void RemoveBoundingBox();
    // Used to activate and deactivate bounding box tool.
    virtual void ToggleBoundingBox();
    
    // Used to set label display mode.
    virtual void SetLabelDisplayMode(LabelsDisplayMode mode);
    // Used to show or hide labels.
    virtual void ShowLabels(bool show);
    // Used to toggle '_displayLabels'.
    virtual void ToggleShowSegmentedLabels();
    
    // Mouse events handling
    virtual void OnMouseLeftDown(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseLeftUp(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseMotion(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseRightDown(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseLeave(wxMouseEvent& event);
    
    // Keyboard events handling
    virtual void OnKeyDown(wxKeyEvent& event);
    
protected:
    // Used to clear contents of 'bitmap' and 'displayBitmap'.
    virtual void ClearBitmap(wxBitmap &bitmap, wxBitmap &displayBitmap);
    // Used to clear _segmentationResultBitmap and _segmentationResultDisplayBitmap.
    virtual void ClearSegmentationResultBitmap();
    // Used to clear _markersBitmap and _markersDisplayBitmap.
    virtual void ClearMarkersBitmap();
    
public:
    /////////////////////////////////////////////////////////
    // Helper methods for converting images, bitmaps, colors
    /////////////////////////////////////////////////////////
    
    // Converting colors and labels
    ///////////////////////////////
    
    // Converts internal label to RGB color for displaying on screen
    static i3d::RGB ConvertLabelToDisplayColor(i3d::GRAY16 label);
    
    // Converts internal BG/FG label to RGB color for displaying on screen
    static i3d::RGB ConvertBinaryLabelToColour(BinaryLabelType label);
    
    // Converts internal GRAY16 label to 3*8bit wxColor
    // so that we can store 16bit in wxImage and wxBitmap
    // GRAY16 is translated to RGB8 as  Red = Gray >> 8, Green = Gray & 0xFF;
    static wxColour ConvertGRAY16LabelToInternalwxColor(i3d::GRAY16 label);
    
    // Converts internal RGB8 label to GRAY16
    // RGB8 is translated to GRAY16 as Gray = (Red << 8) + Green
    static i3d::GRAY16 ConvertInternalRGB8ToGRAY16(i3d::GRAY8 r, i3d::GRAY8 g);
    
    // Converts RGB to wxColour
    static wxColour RGBToWxColour(i3d::RGB color);
    
    // Converting i3d Image and wxImage without color changes
    /////////////////////////////////////////////////////////
    
    // Converts i3d RGB image to RGB wxImage
    static wxImage ConvertImage3dToWxImage(const i3d::Image3d<i3d::RGB> &image);
    // Converts i3d GRAY8 image to RGB wxImage
    static wxImage ConvertImage3dToWxImage(const i3d::Image3d<i3d::GRAY8> &image);
    
    // Converts wxImage to RGB i3d Image
    static i3d::Image3d<i3d::RGB> ConvertWxImageToImage3dRGB(const wxImage &image);
    
    static i3d::Image3d<i3d::GRAY8> ConvertImage3dGRAY16ToGRAY8(const i3d::Image3d<i3d::GRAY16> &image);
    
    
    // Converting i3d Image and wxImage with color translations
    ///////////////////////////////////////////////////////////
    
    // Converts Image3d with GRAY16 labels to wxImage that should be displayed
    // Labels are translated to display RGB8 colors
    static wxImage ConvertLabelImage3dToDisplayWxImage(const i3d::Image3d<i3d::GRAY16> &image, bool binary = false);
    
    // Converts wxImage with 16bit labels encoded as RGB8 to GRAY16 Image3d
    // RGB8 is translated to GRAY16 as Gray = (Red << 8) + Green
    static i3d::Image3d<i3d::GRAY16> ConvertLabelWxImageToImage3dGRAY16(const wxImage &image);
    
    // Converts GRAY16 Image3d with 16bit labels to wxImage with labels encoded as RGB8
    // GRAY16 is translated to RGB8 as  Red = Gray >> 8, Green = Gray & 0xFF;
    static wxImage ConvertImage3dGRAY16ToLabelWxImage(i3d::Image3d<i3d::GRAY16> &image);
    
    // Other helper methods
    ///////////////////////
    
    // Apply alpha to all non-zero pixels in wxImage t
    static void ApplyAlphaToNonZeroPixels(wxImage &image, unsigned int alpha);
    
    // Crops image with given bounding box
    template<class PIXEL> void CropImage(i3d::Image3d<PIXEL> &in, i3d::Image3d<PIXEL> &out, wxRect &bbox){
        if (bbox.GetWidth() == 0  || bbox.GetHeight() == 0) {
            out = in;
        } else {
            i3d::VOI<i3d::PIXELS> voi(bbox.GetPosition().x, bbox.GetPosition().y, 0, bbox.GetWidth(), bbox.GetHeight(), 1);
            out.CopyFromVOI(in, voi);
        }
    }
    
    // Destructor
    ~Segmenter();

};


#endif /* Segmenter_hpp */

