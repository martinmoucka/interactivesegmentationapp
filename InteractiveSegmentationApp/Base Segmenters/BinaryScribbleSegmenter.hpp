//
//  BinaryScribbleSegmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 29.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef BinaryScribbleSegmenter_hpp
#define BinaryScribbleSegmenter_hpp

#include <stdio.h>
#include "ScribbleSegmenter.hpp"

// This class is a specialization of ScribbleSegmenter
// with support of binary segmentation.

class BinaryScribbleSegmenter: public ScribbleSegmenter {
public:
    // Constructor
    BinaryScribbleSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    
    // Override methods
    virtual void Init();
    virtual void AddButtonsToRibbon();
   
    // Event handling
    virtual void OnRibbonButtonClicked(wxRibbonButtonBarEvent& event);
    virtual void OnKeyDown(wxKeyEvent& event);
    virtual void AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels);
    // User interface
    virtual void UpdateUI();
    virtual void ResetUI();

    // Used to set background label.
    // Called when 'Background label' button is clicked.
    virtual void SetBackgroundLabel();
    // Used to set foreground label.
    // Called when 'Foreground label' button is clicked.
    virtual void SetForegroundLabel();

    
    // Define ids for new buttons
    enum {
        ID_BUTTON_BACKGROUND_LABEL = 20001,
        ID_BUTTON_FOREGROUND_LABEL = 20002,
    };
    
};




#endif /* BinaryScribbleSegmenter_hpp */
