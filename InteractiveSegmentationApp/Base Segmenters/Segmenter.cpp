//
//  Segmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 08.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "Segmenter.hpp"
#include <wx/graphics.h>
#include "MainFrame.h"

Segmenter::Segmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame) {
    _id = id;
    _parentFrame = parentFrame;
    _parentRibbonBar = ribbonBar;
    _mouseMode = new SegmenterMouseMode();
}

void Segmenter::Init() {
    AddRibbonPage();
    _ribbonPage->Realize();
    _parentRibbonBar->Realize();
}

// MARK: - User interface

void Segmenter::AddRibbonPage() {
    // Create ribbon page, panel, buttonBar
    _ribbonPage = new wxRibbonPage(_parentRibbonBar, _id, _(_name), wxNullBitmap, 0);
    //_parentRibbonBar->SetActivePage( _ribbonPage );
    AddCommonRibbonPanel();
    wxRibbonPanel *panel = new wxRibbonPanel(_ribbonPage, wxID_ANY, _(_name), wxNullBitmap, wxDefaultPosition, wxDLG_UNIT(_ribbonPage, wxSize(-1,-1)), wxRIBBON_PANEL_DEFAULT_STYLE);
    
    _ribbonButtonBar = new wxRibbonButtonBar(panel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(panel, wxSize(-1,-1)), 0);
    AddButtonsToRibbon();
}

void Segmenter::AddCommonRibbonPanel(){
    wxRibbonPanel *panel = new wxRibbonPanel(_ribbonPage, wxID_ANY, _("Navigation"), wxNullBitmap, wxDefaultPosition, wxDLG_UNIT(_ribbonPage, wxSize(-1,-1)), wxRIBBON_PANEL_DEFAULT_STYLE);
    _commonRibbonButtonBar = new wxRibbonButtonBar(panel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(panel, wxSize(-1,-1)), 0);
    wxSize iconSize(32,32);
    _commonRibbonButtonBar->AddButton( ID_BUTTON_COMMON_PREVIOUS_IMAGE, _("Previous Image"), wxArtProvider::GetBitmap(wxART_GO_BACK, wxART_BUTTON, iconSize), _("Previous Image"), wxRIBBON_BUTTON_NORMAL);
    _commonRibbonButtonBar->AddButton( ID_BUTTON_COMMON_NEXT_IMAGE, _("Next Image"), wxArtProvider::GetBitmap(wxART_GO_FORWARD, wxART_BUTTON, iconSize), _("Next Image"), wxRIBBON_BUTTON_NORMAL);
    _commonRibbonButtonBar->AddButton( ID_BUTTON_COMMON_ZOOM_OUT, _("Zoom Out"), wxArtProvider::GetBitmap(wxART_MINUS, wxART_BUTTON, iconSize), _("Zoom Out"), wxRIBBON_BUTTON_NORMAL);
    _commonRibbonButtonBar->AddButton( ID_BUTTON_COMMON_ZOOM_IN, _("Zoom In"), wxArtProvider::GetBitmap(wxART_PLUS, wxART_BUTTON, iconSize), _("Zoom In"), wxRIBBON_BUTTON_NORMAL);
    _commonRibbonButtonBar->AddButton( ID_BUTTON_COMMON_CURRENT_LABEL, _("Current Label"), wxArtProvider::GetBitmap(wxART_PLUS, wxART_BUTTON, iconSize), _("Zoom InCurrent Label"), wxRIBBON_BUTTON_DROPDOWN);
    _commonRibbonButtonBar->AddButton( ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS, _("Show Labels"), wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON, wxDefaultSize), _("Show Labels"), wxRIBBON_BUTTON_HYBRID);
    
    _commonRibbonButtonBar->Bind(wxEVT_COMMAND_RIBBONBUTTON_CLICKED, &Segmenter::OnRibbonButtonClicked, this, wxID_ANY);
    _commonRibbonButtonBar->Bind(wxEVT_COMMAND_RIBBONBUTTON_DROPDOWN_CLICKED, &Segmenter::OnRibbonButtonDropdownClicked, this, wxID_ANY);
    _commonRibbonButtonBar->Bind(wxEVT_COMMAND_MENU_SELECTED, &Segmenter::OnMenuSelected, this, wxID_ANY);
}


void Segmenter::AddButtonsToRibbon() {
    if (!_ribbonButtonBar) {
        std::cerr << "AddButtonsToRibbon called on Segmenter with no _ribbonButtonBar" << std::endl;
        return;
    }
    
    if (_showBoundingBoxButton) {
        _ribbonButtonBar->AddButton(ID_BUTTON_BOUNDING_BOX, _("Bounding box"), wxArtProvider::GetBitmap(wxART_CUT, wxART_BUTTON, wxDefaultSize), _("Bounding box"), wxRIBBON_BUTTON_TOGGLE);
        _ribbonButtonBar->AddButton(ID_BUTTON_BOUNDING_BOX_RESET, _("Remove bounding box"), wxArtProvider::GetBitmap(wxART_CUT, wxART_BUTTON, wxDefaultSize), _("Remove bounding box"), wxRIBBON_BUTTON_NORMAL);
    }
    
    if (_showClearMarkersButton) {
    _ribbonButtonBar->AddButton(ID_BUTTON_CLEAR_ACTIVE_LABELS, _("Clear Markers"), wxArtProvider::GetBitmap(wxART_CROSS_MARK, wxART_BUTTON, wxDefaultSize), _("Clear Markers"), wxRIBBON_BUTTON_NORMAL);
    }
    
    if (_showRunSegmentationButton) {
         _ribbonButtonBar->AddButton(ID_BUTTON_RUN_SEGMENTATION, _("Run Segmentation (R)"), wxArtProvider::GetBitmap(wxART_GO_FORWARD, wxART_BUTTON, wxDefaultSize), _("Run Segmentation (R)"), wxRIBBON_BUTTON_NORMAL);
    }
   
    if (_showClearSegmentationResultButton) {
        _ribbonButtonBar->AddButton(ID_BUTTON_CLEAR_SEGMENTATION_RESULT, _("Clear Segmentation Result"), wxArtProvider::GetBitmap(wxART_CROSS_MARK, wxART_BUTTON, wxDefaultSize), _("Clear Segmentation Result"), wxRIBBON_BUTTON_NORMAL);
    }
    
    
    if (_showAddToSegmentationButton) {
        _ribbonButtonBar->AddButton(ID_BUTTON_ADD_TO_SEGMENTED_IMAGE, _("Add to Segmentation (A)"), wxArtProvider::GetBitmap(wxART_PLUS, wxART_BUTTON, wxDefaultSize), _("Add to Segmentation"), wxRIBBON_BUTTON_NORMAL);
    }
    
    
    _ribbonButtonBar->Bind(wxEVT_COMMAND_RIBBONBUTTON_CLICKED, &Segmenter::OnRibbonButtonClicked, this, wxID_ANY);
    
}

void Segmenter::RefreshCanvas() {
    _parentFrame->GetImageScrolledWindow()->Refresh();
}

void Segmenter::UpdateUI() {
    _ribbonButtonBar->ToggleButton(ID_BUTTON_BOUNDING_BOX, GetMouseMode()->IsBoundingBox());
}
void Segmenter::ResetUI() {}

void Segmenter::OnRibbonButtonClicked(wxRibbonButtonBarEvent &event)
{
    int id = _ribbonButtonBar->GetItemId(event.GetButton());
    switch (id) {
        case ID_BUTTON_COMMON_PREVIOUS_IMAGE:
            _parentFrame->OnPreviousImage(event);
            break;
        case ID_BUTTON_COMMON_NEXT_IMAGE:
            _parentFrame->OnNextImage(event);
            break;
        case ID_BUTTON_COMMON_ZOOM_OUT:
            _parentFrame->OnZoomOut(event);
            break;
        case ID_BUTTON_COMMON_ZOOM_IN:
            _parentFrame->OnZoomIn(event);
            break;
        case ID_BUTTON_COMMON_CURRENT_LABEL:
            OnCurrentLabelDropDown(event);
            break;
        case ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS :
            ToggleShowSegmentedLabels();
            break;
        case ID_BUTTON_BOUNDING_BOX:
            ToggleBoundingBox();
            break;
        case ID_BUTTON_BOUNDING_BOX_RESET:
            RemoveBoundingBox();
            break;
        case ID_BUTTON_CLEAR_ACTIVE_LABELS:
            ClearMarkersBitmap();
            break;
        case ID_BUTTON_CLEAR_SEGMENTATION_RESULT:
            ClearSegmentationResultBitmap();
            break;
        case ID_BUTTON_RUN_SEGMENTATION:
            OnRunSegmentation();
            break;
        case ID_BUTTON_ADD_TO_SEGMENTED_IMAGE:
            AddSegmentationResultToSegmentedImageWrapper();
            break;
        
        default:
            break;
    }
}

void Segmenter::OnRibbonButtonDropdownClicked(wxRibbonButtonBarEvent &event) {
    int id = _ribbonButtonBar->GetItemId(event.GetButton());
     switch (id) {
         case ID_BUTTON_COMMON_CURRENT_LABEL:
             OnCurrentLabelDropDown(event);
             break;
         case ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS:
             OnShowLabelsDropDown(event);
             break;
     }
}


// MARK: - Image handling

void Segmenter::SetImage(std::shared_ptr<SegmentedImage> image, std::string name) {
    _segmentedImage = image;
    ClearMarkersBitmap();
    ClearSegmentationResultBitmap();
}

i3d::Image3d<i3d::GRAY16> Segmenter::GetLabeledImage() {
    return _segmentedImage->GetLabeledImage();
}

i3d::GRAY16 Segmenter::GetMaxUsedLabel() {
    if (_segmentedImage == NULL) {
        return 0;
    } else {
        return _segmentedImage->GetMaxUsedLabel();
    }
};

void Segmenter::SetLabel(i3d::GRAY16 label, wxBitmap bmp) {
    _currentLabel = label;

    _commonRibbonButtonBar->DeleteButton(ID_BUTTON_COMMON_CURRENT_LABEL);
    
    _commonRibbonButtonBar->AddButton( ID_BUTTON_COMMON_CURRENT_LABEL, _("Current Label"), bmp, _("Current Label"), wxRIBBON_BUTTON_DROPDOWN);
    _commonRibbonButtonBar->Realize();
}

wxBitmap Segmenter::GetDisplayBitmap(){
    if (!_segmentedImage) {
        return wxBitmap();
    }
    return _segmentedImage->GetOriginalBitmap();
}


void Segmenter::Render(wxDC &dc) {
	wxBitmap bitmap = GetDisplayBitmap();
	if (!bitmap.IsOk()) return;
	wxBitmap drawBitmap(bitmap);
	wxMemoryDC memDc(drawBitmap);

	if (_displayLabels) {
        if(_labelsDisplayMode == Fill) {
            memDc.DrawBitmap(_segmentedImage->GetLabeledBitmap(), 0, 0, true);
        } else if(_labelsDisplayMode == Outline) {
            memDc.DrawBitmap(_segmentedImage->GetLabeledOutlinedBitmap(), 0, 0, true);
        }
	}

	if (_segmentationResultDisplayBitmap.IsOk()) {
		memDc.DrawBitmap(_segmentationResultDisplayBitmap, 0, 0, true);
	}

	if (_markersDisplayBitmap.IsOk()) {
		memDc.DrawBitmap(_markersDisplayBitmap, 0, 0, true);
	}

	dc.Blit(0, 0, bitmap.GetWidth(), bitmap.GetHeight(), &memDc, 0, 0);
    DrawBox(dc);
}

void Segmenter::DrawBox(wxDC& dc) {
    
    if (_bbox.GetWidth() == 0 || _bbox.GetHeight() == 0) {
        return;
    }
    wxGraphicsContext *gc = wxGraphicsContext::CreateFromUnknownDC(dc);
    gc->SetPen(wxPen(wxColour(255, 255, 160, 255), 3, wxPENSTYLE_SOLID));
    gc->SetBrush(wxBrush(wxColour(0, 0, 0, 0)));
    gc->DrawRectangle(_bbox.GetX(), _bbox.GetY(), _bbox.GetWidth(), _bbox.GetHeight());
    delete gc;
}

// MARK: - Mouse events handling

void Segmenter::OnMouseLeftDown(wxMouseEvent &event, bool insideImage){
    if (GetMouseMode()->IsBoundingBox() && insideImage) {
        _bbox = wxRect(event.GetPosition(), event.GetPosition());
    }
    RefreshCanvas();
}

void Segmenter::OnMouseLeftUp(wxMouseEvent &event, bool insideImage){
    RefreshCanvas();
}

void Segmenter::OnMouseMotion(wxMouseEvent &event, bool insideImage){
    if (GetMouseMode()->IsBoundingBox() && insideImage && event.LeftIsDown()) {
        _bbox.SetBottomRight(event.GetPosition());
    }
    RefreshCanvas();
}

void Segmenter::OnMouseRightDown(wxMouseEvent &event, bool insideImage) {
    if (!insideImage) return;
    _lastRightClick = event.GetPosition();
    ShowRightClickMenu();
}

void Segmenter::OnMouseLeave(wxMouseEvent &event) {
    RefreshCanvas();
}

void Segmenter::OnKeyDown(wxKeyEvent &event) {
    switch (event.GetKeyCode()) {
        case 'R':
            if (_segmentedImage != NULL) {
                OnRunSegmentation();
            }
            break;
        case 'A':
            if (_segmentedImage != NULL) {
                AddSegmentationResultToSegmentedImageWrapper();
            }
            break;
        default:
            event.Skip();
            break;
    }
}

// MARK: - Event handling

void Segmenter::OnRepaintComponent(){
    _segmentedImage->RepaintComponent(_lastRightClick.x, _lastRightClick.y, _currentLabel);
    RefreshCanvas();
}

void Segmenter::OnRepaintAllComponents(){
    _segmentedImage->RepaintAllComponents(_lastRightClick.x, _lastRightClick.y, _currentLabel);
    RefreshCanvas();
}

void Segmenter::OnSelectLabel() {
    int x = _lastRightClick.x;
    int y = _lastRightClick.y;
    wxImage markersImg = _markersBitmap.ConvertToImage();
    i3d::GRAY16 label = _segmentedImage->GetLabeledImage().GetVoxel(x, y, 0);

    i3d::GRAY16 markersLabel = ConvertInternalRGB8ToGRAY16(markersImg.GetRed(x, y), markersImg.GetGreen(x, y));
    
    if (_type == Nary && markersLabel != 0) {
        label = markersLabel;
    }
    if (label != 0) {
        _parentFrame->SetLabelToAllSegmenters(label);
    }
    
    printf("Label: [%d] \n", label );
}

void Segmenter::OnCurrentLabelDropDown(wxRibbonButtonBarEvent& evt) {
    wxMenu menu;
    menu.Append(ID_BUTTON_COMMON_NEXT_UNUSED_LABEL, wxT("Set unused label (U)"));
    menu.Append(ID_BUTTON_COMMON_BACKGROUND_LABEL, wxT("Set background label (B)"));
    evt.PopupMenu(&menu);
}

void Segmenter::OnShowLabelsDropDown(wxRibbonButtonBarEvent& evt) {
    wxMenu menu;
    menu.AppendCheckItem(ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_FILL, wxT("Show filled labels (F)"));
    menu.AppendCheckItem(ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_OUTLINE, wxT("Show outlined labels (O)"));
    menu.AppendCheckItem(ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_HIDE, wxT("Hide labels (H)"));
    menu.Check(ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_FILL, _labelsDisplayMode == Fill && _displayLabels);
    menu.Check(ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_OUTLINE, _labelsDisplayMode == Outline && _displayLabels);
    menu.Check(ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_HIDE, !_displayLabels);
    evt.PopupMenu(&menu);
}

void Segmenter::ShowRightClickMenu() {
    wxMenu menu;
    menu.Append(ID_RIGHT_CLICK_MENU_REPAINT_COMPONENT, wxT("Repaint this component with current label"), wxT("Repaint component with current label"));
    menu.Append(ID_RIGHT_CLICK_MENU_REPAINT_ALL_COMPONENTS, wxT("Repaint this label's components with current label"), wxT("Repaint this label's components with current label"));
    menu.Append(ID_RIGHT_CLICK_MENU_SELECT_LABEL, wxT("Select this label"), wxT("Select this label"));
    auto pos = wxGetMousePosition();
    auto framePos = _parentFrame->GetScreenPosition();
    _parentFrame->PopupMenu(&menu, pos.x - framePos.x, pos.y - framePos.y);
}

void Segmenter::OnMenuSelected(wxCommandEvent &event){
    auto id = event.GetId();
    switch (id) {
        case ID_BUTTON_COMMON_NEXT_UNUSED_LABEL:
            _parentFrame->SetUnusedLabelToAllSegmenters();
            break;
        case ID_BUTTON_COMMON_BACKGROUND_LABEL:
            _parentFrame->SetLabelToAllSegmenters(0);
            break;
        case ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_FILL:
            SetLabelDisplayMode(Fill);
            break;
        case ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_OUTLINE:
            SetLabelDisplayMode(Outline);
            break;
        case ID_BUTTON_COMMON_SHOW_SEGMENTED_LABELS_MENU_HIDE:
            ShowLabels(false);
            break;
        default:
            break;
    }
    
}

void Segmenter::OnRunSegmentation() {
    
    i3d::Image3d<i3d::GRAY16> mask =  ConvertLabelWxImageToImage3dGRAY16(_markersBitmap.ConvertToImage());
    i3d::Image3d<i3d::GRAY16> croppedMask;
    wxRect bbox = FixRect(_bbox);
    i3d::Image3d<i3d::GRAY16> result;
    bool needsCropping = true;
    if (bbox.GetWidth() == 0  || bbox.GetHeight() == 0) {
        needsCropping = false;
        bbox = wxRect(0, 0, mask.GetWidth(), mask.GetHeight());
    }
    
    if (_segmentedImage->IsGray8()) {
        auto img = _segmentedImage->GetOriginalGrayImage();
        i3d::Image3d<i3d::GRAY8> croppedImg;
        if (!needsCropping) {
            croppedImg = img;
            croppedMask = mask;
        } else {
            CropImage(img, croppedImg, bbox);
            CropImage(mask, croppedMask, bbox);
        }
        result = RunSegmentation(croppedImg, croppedMask);
    } else {
        auto img = _segmentedImage->GetOriginalRgbImage();
        i3d::Image3d<i3d::RGB> croppedImg;
        if (!needsCropping) {
            croppedImg = img;
            croppedMask = mask;
        } else {
            CropImage(img, croppedImg, bbox);
            CropImage(mask, croppedMask, bbox);
        }
        result = RunSegmentation(croppedImg, croppedMask);
    }

    ClearSegmentationResultBitmap();
    // Draw result to label bitmap
    _segmentationResultBitmap = wxBitmap(wxImage(_segmentedImage->GetWidth(), _segmentedImage->GetHeight()));
    wxImage resultImage = Segmenter::ConvertImage3dGRAY16ToLabelWxImage(result);
    resultImage.SetMaskColour(0, 0, 0);
    wxBitmap resultBitmap(resultImage);
    wxMemoryDC ldc(_segmentationResultBitmap);
    ldc.DrawBitmap(resultBitmap, bbox.GetTopLeft(), true);
    //Draw result to displayed label bitmap
    wxImage displayLabelImage = Segmenter::ConvertLabelImage3dToDisplayWxImage(result, (_type == Binary) );
    
    ApplyAlphaToNonZeroPixels(displayLabelImage, 120);

    wxMemoryDC dc(_segmentationResultDisplayBitmap);
    wxGraphicsContext *gc = wxGraphicsContext::Create(dc);
    wxBitmap displayLabelBitmap(displayLabelImage);
    gc->DrawBitmap(displayLabelBitmap, bbox.GetX(), bbox.GetY(), displayLabelImage.GetWidth(), displayLabelImage.GetHeight());
    delete gc;
    RefreshCanvas();
}

void Segmenter::AddSegmentationResultToSegmentedImageWrapper() {
    auto labels = ConvertLabelWxImageToImage3dGRAY16(_segmentationResultBitmap.ConvertToImage());
   AddSegmentationResultToSegmentedImage(labels);
    ClearMarkersBitmap();
    ClearSegmentationResultBitmap();
    RefreshCanvas();
    ResetUI();
}

void Segmenter::ToggleBoundingBox() {
    if (GetMouseMode()->IsBoundingBox()) {
        GetMouseMode()->SetNone();
    } else {
        GetMouseMode()->SetBoundingBox();
    }
    UpdateUI();
}

void Segmenter::RemoveBoundingBox() {
    _bbox = wxRect(0,0,0,0);
    RefreshCanvas();
}

void Segmenter::SetLabelDisplayMode(LabelsDisplayMode mode) {
    _displayLabels = true;
    _labelsDisplayMode = mode;
    RefreshCanvas();
}

void Segmenter::ShowLabels(bool show) {
    _displayLabels = show;
    RefreshCanvas();
}

void Segmenter::ToggleShowSegmentedLabels(){
    _displayLabels = !_displayLabels;
    RefreshCanvas();
}

void Segmenter::ClearMarkersBitmap() {
    ClearBitmap(_markersBitmap, _markersDisplayBitmap);
}

void Segmenter::ClearSegmentationResultBitmap() {
    ClearBitmap(_segmentationResultBitmap, _segmentationResultDisplayBitmap);
}

void Segmenter::ClearBitmap(wxBitmap &bitmap, wxBitmap &displayBitmap) {
    if (_segmentedImage == NULL) {
        return;
    }
	int w = (int)_segmentedImage->GetWidth();
	int h = (int)_segmentedImage->GetHeight();
	wxImage img(w, h);
	
	unsigned char* alphaData = new unsigned char[w*h];
	unsigned char* data = new unsigned char[w*h*3];
	memset(alphaData, wxIMAGE_ALPHA_TRANSPARENT, w*h);
	memset(data, 0, w*h*3);
	img.SetData(data);
	img.SetAlpha(alphaData);
	bitmap = wxBitmap(img, 32);
	displayBitmap = wxBitmap(img, 32);
    RefreshCanvas();
}

i3d::RGB Segmenter::ColorFromLabel(i3d::GRAY16 label) {
    if (_type == Binary) {
        return ConvertBinaryLabelToColour( (label == Foreground) ? Foreground : Background );
    } else {
        return ConvertLabelToDisplayColor(label);
    }
    
}

// MARK: - Helper methods

i3d::RGB Segmenter::ConvertLabelToDisplayColor(i3d::GRAY16 label) {
    if (label > 0)
    {
        label = (label - 1) % 15;
        
        switch (label)
        {
            case 0: return i3d::RGB_generic<i3d::GRAY8>(255,   0,   0); break; // red
            case 1: return i3d::RGB_generic<i3d::GRAY8>(255, 255,   0); break; // yellow
            case 2: return i3d::RGB_generic<i3d::GRAY8>(  0, 255,   0); break; // green
            case 3: return i3d::RGB_generic<i3d::GRAY8>(255, 128, 255); break; // light pink
            case 4: return i3d::RGB_generic<i3d::GRAY8>(  0,   0, 255); break; // blue
            case 5: return i3d::RGB_generic<i3d::GRAY8>(  0, 255, 255); break; // cyan
            case 6: return i3d::RGB_generic<i3d::GRAY8>(128,   0,   0); break; // dark red
            case 7: return i3d::RGB_generic<i3d::GRAY8>(  0, 128,   0); break; // dark green
            case 8: return i3d::RGB_generic<i3d::GRAY8>(  0,   0, 128); break; // dark blue
            case 9: return i3d::RGB_generic<i3d::GRAY8>(255, 128,   0); break; // orange
            case 10: return i3d::RGB_generic<i3d::GRAY8>(128, 128, 255); break; // light blue
            case 11: return i3d::RGB_generic<i3d::GRAY8>(128, 128,   0); break; // beuge
            case 12: return i3d::RGB_generic<i3d::GRAY8>(128, 0,   255); break; // dark magenta
            case 13: return i3d::RGB_generic<i3d::GRAY8>(255, 128, 255); break; // light pink
            case 14: return i3d::RGB_generic<i3d::GRAY8>(128, 128, 128); break; // grey
        }
    }
    
    return i3d::RGB_generic<i3d::GRAY8>(0, 0, 0);
}

i3d::RGB Segmenter::ConvertBinaryLabelToColour(Segmenter::BinaryLabelType label) {
    switch (label) {
        case Background:
            return i3d::RGB_generic<i3d::GRAY8>(1, 1, 1); break;
        case Foreground:
            return i3d::RGB_generic<i3d::GRAY8>(255, 230, 0); break;
    }
}

wxColour Segmenter::RGBToWxColour(i3d::RGB color) {
    return wxColour(color.red, color.green, color.blue);
}


wxImage Segmenter::ConvertImage3dToWxImage(const i3d::Image3d<i3d::RGB> &image) {
    size_t width = image.GetSizeX();
    size_t height = image.GetSizeY();
    std::valarray<i3d::RGB> data = image.GetVoxelData();
    unsigned char *rawData = (unsigned char *) malloc(3*width*height*sizeof(unsigned char));
    size_t index = 0;
    for (auto it = begin(data); it != end(data); ++it) {
        rawData[index*3] = it->red;
        rawData[index*3 + 1] = it->green;
        rawData[index*3 + 2] = it->blue;
        index++;
    }
    return wxImage((int)width, (int)height, rawData, false);
}

wxImage Segmenter::ConvertImage3dToWxImage(const i3d::Image3d<i3d::GRAY8> &image) {
    size_t width = image.GetSizeX();
    size_t height = image.GetSizeY();
    std::valarray<i3d::GRAY8> data = image.GetVoxelData();
    unsigned char *rawData = (unsigned char *) malloc(3*width*height*sizeof(unsigned char));
    size_t index = 0;
    for (auto it = begin(data); it != end(data); ++it) {
        rawData[index*3] = *it;
        rawData[index*3 + 1] = *it;
        rawData[index*3 + 2] = *it;
        index++;
    }
    return wxImage((int)width, (int)height, rawData, false);
}

wxImage Segmenter::ConvertLabelImage3dToDisplayWxImage(const i3d::Image3d<i3d::GRAY16> &image, bool binary) {
    size_t width = image.GetSizeX();
    size_t height = image.GetSizeY();
    std::valarray<i3d::GRAY16> dataIn = image.GetVoxelData();
    unsigned char *dataOut = (unsigned char *) malloc(3*width*height*sizeof(unsigned char));
    size_t index = 0;
    
    if (binary) {
        for (auto it = begin(dataIn); it != end(dataIn); ++it) {
            i3d::RGB color;
                if( *it == Foreground ) { color = ConvertBinaryLabelToColour(Foreground); }
                else { color = ConvertLabelToDisplayColor(0); }
            dataOut[index++] = color.red;
            dataOut[index++] = color.green;
            dataOut[index++] = color.blue;
        }
    } else {
        for (size_t i = 0; i < image.GetImageSize(); ++i){
            i3d::RGB color = ConvertLabelToDisplayColor(image.GetVoxel(i));
            dataOut[index++] = color.red;
            dataOut[index++] = color.green;
            dataOut[index++] = color.blue;
        }
    }
    return wxImage((int)width, (int)height, dataOut, false);
}

i3d::Image3d<i3d::RGB> Segmenter::ConvertWxImageToImage3dRGB(const wxImage &image) {
    
    auto data = image.GetData();
    int width = image.GetWidth();
    int height = image.GetHeight();
    i3d::Image3d<i3d::RGB> img;
    img.MakeRoom(width, height, 1);
    size_t index = 0;
    for (auto it = img.begin(); it != img.end(); ++it) {
        it->red = data[index*3];
        it->green = data[index*3 + 1];
        it->blue = data[index*3 + 2];
        index++;
    }
    return img;
}

i3d::Image3d<i3d::GRAY8> Segmenter::ConvertImage3dGRAY16ToGRAY8(const i3d::Image3d<i3d::GRAY16> &image) {
    i3d::Image3d<i3d::GRAY8> out;
    out.MakeRoom(image.GetWidth(), image.GetHeight(), 1);
    for (size_t i = 0; i < image.GetImageSize(); ++i) {
        out.SetVoxel(i, image.GetVoxel(i)/256);
    }
    return out;
}

i3d::Image3d<i3d::GRAY16> Segmenter::ConvertLabelWxImageToImage3dGRAY16(const wxImage &image) {
    
    auto data = image.GetData();
    int width = image.GetWidth();
    int height = image.GetHeight();
    i3d::Image3d<i3d::GRAY16> img;
    img.MakeRoom(width, height, 1);
    size_t index = 0;
    for (auto it = img.begin(); it != img.end(); ++it) {
        auto r = data[index*3];
        auto g = data[index*3 + 1];
        *it = (r<<8) + g;
        index++;
    }
    return img;
}

wxImage Segmenter::ConvertImage3dGRAY16ToLabelWxImage(i3d::Image3d<i3d::GRAY16> &image) {
    size_t width = image.GetSizeX();
    size_t height = image.GetSizeY();
    std::valarray<i3d::GRAY16> dataIn = image.GetVoxelData();
    unsigned char *dataOut = (unsigned char *) malloc(3*width*height*sizeof(unsigned char));
    size_t index = 0;
    for (auto it = begin(dataIn); it != end(dataIn); ++it) {
        unsigned char r = *it >> 8, g = *it & 0xFF;
        dataOut[index*3] = r;
        dataOut[index*3 + 1] = g;
        dataOut[index*3 + 2] = 0;
        index++;
    }
    return wxImage((int)width, (int)height, dataOut, false);
}

wxColour Segmenter::ConvertGRAY16LabelToInternalwxColor(i3d::GRAY16 label){
    return wxColour(label >> 8, label & 0xFF, 0, 255);
}

i3d::GRAY16 Segmenter::ConvertInternalRGB8ToGRAY16(i3d::GRAY8 r, i3d::GRAY8 g) {
    return  ( r << 8 ) + g;
}

void Segmenter::ApplyAlphaToNonZeroPixels(wxImage &img, unsigned int alpha) {
    
    size_t n = img.GetWidth() * img.GetHeight();
    unsigned char *alphaData = (unsigned char *) malloc(n*sizeof(unsigned char));
    unsigned char *imgData = img.GetData();
    for (size_t i = 0; i < n; ++i){
        unsigned char r = imgData[3*i];
        unsigned char g = imgData[3*i + 1];
        unsigned char b = imgData[3*i + 2];
        if (r == 0 && g == 0 && b == 0) {
            alphaData[i] = 0;
        } else {
            alphaData[i] = alpha;
        }
    }
    img.SetAlpha(alphaData);
}

Segmenter::~Segmenter() {
    int pageNumber = _parentRibbonBar->GetPageNumber(_ribbonPage);
    _parentRibbonBar->DeletePage(pageNumber);
    _parentRibbonBar->Realize();
}
