//
//  NaryScribbleSegmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 29.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef NaryScribbleSegmenter_hpp
#define NaryScribbleSegmenter_hpp

#include <stdio.h>
#include "ScribbleSegmenter.hpp"

// This class is a specialization of ScribbleSegmenter
// with support of N-ary segmentation.

class NaryScribbleSegmenter: public ScribbleSegmenter {
public:
    // Constructor
    NaryScribbleSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    
    // Override methods
    virtual void AddButtonsToRibbon();
    virtual void OnRibbonButtonClicked(wxRibbonButtonBarEvent& event);
    virtual void AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels);
    
    // Used to set previous label.
    // Called when 'Previous label' button is clicked.
    void SetPreviousLabel();
    // Used to set next label.
    // Called when 'Next label' button is clicked.
    void SetNextLabel();
    
    // Define ids for new buttons.
    enum {
        ID_BUTTON_PREVIOUS_LABEL = 10001,
        ID_BUTTON_NEXT_LABEL = 10002,
    };
};

#endif /* NaryScribbleSegmenter_hpp */
