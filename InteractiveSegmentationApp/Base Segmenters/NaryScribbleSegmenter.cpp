//
//  NaryScribbleSegmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 29.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "NaryScribbleSegmenter.hpp"
#include "MainFrame.h"


NaryScribbleSegmenter::NaryScribbleSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame):
ScribbleSegmenter(id, ribbonBar, parentFrame) {
    _name = "N-ary Scribble Segmenter";
    _type = Nary;
}


void NaryScribbleSegmenter::AddButtonsToRibbon() {
    if (!_ribbonButtonBar) {
        std::cerr << "AddButtonsToRibbon called on Segmenter with no _ribbonButtonBar" << std::endl;
        return;
    }
    
    
    _ribbonButtonBar->AddButton(ID_BUTTON_PREVIOUS_LABEL, _("Previous Label (F1)"), wxArtProvider::GetBitmap(wxART_GO_BACK, wxART_BUTTON, wxDefaultSize), _("Previous Label"), wxRIBBON_BUTTON_NORMAL);
    
    _ribbonButtonBar->AddButton(ID_BUTTON_NEXT_LABEL, _("Next Label (F2)"), wxArtProvider::GetBitmap(wxART_GO_FORWARD, wxART_BUTTON, wxDefaultSize), _("Next Label"), wxRIBBON_BUTTON_NORMAL);
    
    ScribbleSegmenter::AddButtonsToRibbon();
}

void NaryScribbleSegmenter::OnRibbonButtonClicked(wxRibbonButtonBarEvent &event)
{
    int id = _ribbonButtonBar->GetItemId(event.GetButton());
    switch (id) {
        case ID_BUTTON_PREVIOUS_LABEL:
            SetPreviousLabel();
            break;
        case ID_BUTTON_NEXT_LABEL:
            SetNextLabel();
            break;
        default:
            ScribbleSegmenter::OnRibbonButtonClicked(event);
            break;
    }
}

void NaryScribbleSegmenter::SetPreviousLabel() {
    if (_currentLabel >= 1) {
        _currentLabel = _currentLabel - 1;
        _parentFrame->SetLabelToAllSegmenters(_currentLabel);
    }
}

void NaryScribbleSegmenter::SetNextLabel() {
    if (_currentLabel < std::numeric_limits<i3d::GRAY16>::max() - 1) {
        _currentLabel = _currentLabel + 1;
        _parentFrame->SetLabelToAllSegmenters(_currentLabel);
    }
}

void NaryScribbleSegmenter::AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels) {
    _segmentedImage->AddLabels(labels);
}
