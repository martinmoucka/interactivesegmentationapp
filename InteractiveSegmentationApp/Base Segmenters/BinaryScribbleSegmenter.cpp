//
//  BinaryScribbleSegmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 29.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "BinaryScribbleSegmenter.hpp"
#include "MainFrame.h"

BinaryScribbleSegmenter::BinaryScribbleSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame):
ScribbleSegmenter(id, ribbonBar, parentFrame) {
    _name = "Binary Scribble Segmenter";
    _type = Binary;
}

void BinaryScribbleSegmenter::Init() {
    // Add Ribbon page
    ScribbleSegmenter::Init();

    _drawingLabel = Background;

}

void BinaryScribbleSegmenter::AddButtonsToRibbon() {
    
    _ribbonButtonBar->AddButton(ID_BUTTON_BACKGROUND_LABEL, _("Background (F5)"), wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON, wxDefaultSize), _("Background label"), wxRIBBON_BUTTON_TOGGLE);
    
    _ribbonButtonBar->AddButton(ID_BUTTON_FOREGROUND_LABEL, _("Foreground (F6)"), wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON, wxDefaultSize), _("Foreground label"), wxRIBBON_BUTTON_TOGGLE);
    
    ScribbleSegmenter::AddButtonsToRibbon();
}

void BinaryScribbleSegmenter::OnRibbonButtonClicked(wxRibbonButtonBarEvent &event)
{
    int id = _ribbonButtonBar->GetItemId(event.GetButton());
    switch (id) {
        case ID_BUTTON_BACKGROUND_LABEL:
            SetBackgroundLabel();
            break;
        case ID_BUTTON_FOREGROUND_LABEL:
            SetForegroundLabel();
            break;
        default:
            ScribbleSegmenter::OnRibbonButtonClicked(event);
            break;
    }
}

void BinaryScribbleSegmenter::OnKeyDown(wxKeyEvent &event) {
    switch (event.GetKeyCode()) {
        case WXK_F5:
            SetBackgroundLabel();
            break;
        case WXK_F6:
            SetForegroundLabel();
            break;
        default:
            ScribbleSegmenter::OnKeyDown(event);
            break;
    }
}

void BinaryScribbleSegmenter::SetBackgroundLabel() {
    GetMouseMode()->SetScribble();
    _drawingLabel = Segmenter::BinaryLabelType::Background;
    UpdateUI();
}

void BinaryScribbleSegmenter::SetForegroundLabel() {
    GetMouseMode()->SetScribble();
    _drawingLabel = Segmenter::BinaryLabelType::Foreground;
    UpdateUI();
}

void BinaryScribbleSegmenter::UpdateUI() {
    ScribbleSegmenter::UpdateUI();
    if (GetMouseMode()->IsScribble()) {
        _ribbonButtonBar->ToggleButton(ID_BUTTON_BACKGROUND_LABEL, _drawingLabel == Background);
        _ribbonButtonBar->ToggleButton(ID_BUTTON_FOREGROUND_LABEL, _drawingLabel == Foreground);
    } else {
        _ribbonButtonBar->ToggleButton(ID_BUTTON_BACKGROUND_LABEL, false);
        _ribbonButtonBar->ToggleButton(ID_BUTTON_FOREGROUND_LABEL, false);
    }
}

void BinaryScribbleSegmenter::ResetUI() {
    ScribbleSegmenter::ResetUI();
    _parentFrame->SetUnusedLabelToAllSegmenters();
}

void BinaryScribbleSegmenter::AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels) {
    _segmentedImage->AddBinaryLabel(labels, _currentLabel);
}


