//
//  RandomWalkSegmenterBackend.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 22.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef RandomWalkSegmenterBackend_hpp
#define RandomWalkSegmenterBackend_hpp

#include <stdio.h>
#include <vector>
#include <i3d/image3d.h>
#include <armadillo>
#include "Util.hpp"

#endif /* RandomWalkSegmenterBackend_hpp */

using namespace arma;
using namespace std;

// Class implementing the computation step of the Random Walks segmentation technique.

template <class PIXEL> class RandomWalkSegmenterBackend{
protected:
    i3d::Image3d<PIXEL> _image; // Original image.
    i3d::Image3d<i3d::GRAY16> _mask; // Labeled mask with seed points
    size_t _pixelCount;
    double _sigma = 5.0;
    i3d::GRAY16 _bgConstant = std::numeric_limits<i3d::GRAY16>::max();

public:
    // Constructor
    // 'image' is original image
    // 'mask' should contain labeled seed-points. Zero values are ignored. Background label should be indicated with value 65535.
    RandomWalkSegmenterBackend(i3d::Image3d<PIXEL> &image, i3d::Image3d<i3d::GRAY16> mask);
    // Used to set sigma parameter
    void SetSigma(double sigma) {_sigma = sigma;}
    
    // Computes segmentation and returns labeled image. Background label has 0 value.
    i3d::Image3d<i3d::GRAY16> RunSegmentation() {
        std::vector<size_t> seedPositions, notSeedPositions;
        GetSeedPositions(seedPositions, notSeedPositions);
        std::vector<size_t> permInd, permInvInd;

        SubstituteBackgroundConstantWithZero(seedPositions);

        if (seedPositions.size() == 0) {
            return _mask;
        }

        GetPermutationIndices(seedPositions, notSeedPositions, permInd, permInvInd);

        sp_mat L(_pixelCount, _pixelCount);
        ComputeLaplacian(L, permInd, permInvInd);
        sp_mat M = ComputeM(seedPositions);
        size_t nS = seedPositions.size(), nU = notSeedPositions.size();
        sp_mat Bt = L.submat(nS, 0, nS + nU - 1, nS - 1);

        sp_mat Lu = L.submat(nS, nS, nS + nU - 1, nS + nU - 1);

        sp_mat rightSide = (-Bt)*M;
        mat rightSideDense =  mat(rightSide);
        superlu_opts settings;
        settings.refine      = superlu_opts::REF_NONE;
        mat X = spsolve(Lu, rightSideDense, "superlu", settings);

        // Extract labels
        i3d::Image3d<i3d::GRAY16> segmentation = CreateResultLabels(X, notSeedPositions);

        return segmentation;
    }
    
private:
    
    // Computes Laplacian matrix.
    void ComputeLaplacian(sp_mat &L, std::vector<size_t> &permInd, std::vector<size_t> &permInverseInd) {
        
        size_t w = _image.GetWidth();
        size_t h = _image.GetHeight();
        size_t lastRowStart = w*h - w;
        
        // Iterate through all permutation indices
        for(size_t LIdx = 0; LIdx < permInd.size(); ++LIdx) {
            size_t pIdx = permInd.at(LIdx); // Pixel index given by permutation
            size_t bottomPIdx = pIdx+w, rightPIdx = pIdx + 1; // Bottom pixel index and right pixel index
            
            double degreeBottom = 0, degreeRight = 0;
            auto iValue = _image.GetVoxel(pIdx);


            if (pIdx%w != w - 1 /* NOT last column*/) {
            // Look right
                size_t rightLIdx = permInverseInd.at(rightPIdx);
                auto rightValue = _image.GetVoxel(rightPIdx);
                double dist = IntensityDistance(iValue, rightValue);
                double cap = ComputeNeighbourCapacity(dist);
                L(LIdx, rightLIdx) = -cap;
                L(rightLIdx, LIdx) = -cap;
                degreeRight = cap;
                L(rightLIdx, rightLIdx) = L(rightLIdx, rightLIdx) + degreeRight;
            }
            
            if (pIdx < lastRowStart /* NOT last row*/) {
                // Look down
                size_t bottomLIdx = permInverseInd.at(bottomPIdx);
                auto bottomValue = _image.GetVoxel(bottomPIdx);
                double dist = IntensityDistance(iValue, bottomValue);
                double cap = ComputeNeighbourCapacity(dist);
                L(LIdx, bottomLIdx) = -cap;
                L(bottomLIdx, LIdx) = -cap;
                degreeBottom = cap;
                L(bottomLIdx, bottomLIdx) = L(bottomLIdx, bottomLIdx) + degreeBottom;
            }
            
            L(LIdx, LIdx) = L(LIdx, LIdx) + (degreeRight + degreeBottom);
        }
        
    }
    
    // Extracts indices of seed pixels and not-seed pixels from mask.
    void GetSeedPositions(std::vector<size_t> &seedPositions, std::vector<size_t> &notSeedPositions) {
        seedPositions.reserve(1000);
        notSeedPositions.reserve(_pixelCount);
        // Get indices of seedPoints (pixels marked as labels)
        size_t idx = 0;
        for (auto it = _mask.begin(); it != _mask.end(); ++it) {
            if(*it != 0) {
                seedPositions.push_back(idx);
            } else {
                notSeedPositions.push_back(idx);
            }
            ++idx;
        }
    }

    // Substitutes 65535 in mask with 0.
    void SubstituteBackgroundConstantWithZero(std::vector<size_t> &seedPositions) {
        for (auto it = seedPositions.begin(); it != seedPositions.end(); ++it) {
            if (_mask.GetVoxel(*it) == _bgConstant) _mask.SetVoxel(*it, 0);
        }
    }
    
    // Compute permutation indices so that we can reorder laplacian matrix, such that
    // seeded pixels are first and unseeded pixels are second.
    // 'seedPositions' should contain sorted indices of seed pixels in mask.
    // 'notSeedPositions' should contain sorted indices of not-seed pixels in mask.
    // 'permInd' will contain [seedPositions, notSeedPositions]
    // 'permInverseInd' will contain indices of each value in 'permInd'
    void GetPermutationIndices(std::vector<size_t> &seedPositions,
                               std::vector<size_t> &notSeedPositions,
                               std::vector<size_t> &permInd,
                               std::vector<size_t> &permInverseInd) {
        
        permInd.insert(permInd.begin(), seedPositions.begin(), seedPositions.end());
        permInd.insert(permInd.end(), notSeedPositions.begin(), notSeedPositions.end());
        
        permInverseInd.resize(permInd.size());
        
        // Fill vector with inverse indices
        for(size_t i = 0; i < permInd.size(); ++i){
            permInverseInd.at( permInd.at(i) ) = i;
        }
    }
    
    // Compute M matrix
    sp_mat ComputeM(std::vector<size_t> &seedPositions) {
        size_t numLabels = _mask.GetMaxValue() + 1;
        sp_mat M(seedPositions.size() , numLabels);
        size_t seedPointIdx = 0;
        for (auto it = seedPositions.begin(); it != seedPositions.end(); ++it) {
            auto label = _mask.GetVoxel(*it);
            M(seedPointIdx, label) = 1;
            ++seedPointIdx;
        }
        return M;
    }
    
    // Extract resulting labels from computed probabilities
    i3d::Image3d<i3d::GRAY16> CreateResultLabels(mat X, std::vector<size_t> &notSeedPositions) {
        
        i3d::Image3d<i3d::GRAY16> segmentation(_mask);
        
        size_t idx = 0;
        for (auto pos = notSeedPositions.begin(); pos != notSeedPositions.end(); ++pos) {
            auto label = X.row(idx).index_max();
            segmentation.SetVoxel(*pos, label);
            ++idx;
        }
        
        return segmentation;
    }
    
    // Computes Weight of edge for given difference 'd' between pixels.
    double ComputeNeighbourCapacity(double d) {
        double res = std::exp( -( (d)/(_sigma) ) );
        return res;
    }
    
};

template <class PIXEL>
RandomWalkSegmenterBackend<PIXEL>::RandomWalkSegmenterBackend(i3d::Image3d<PIXEL> &image, i3d::Image3d<i3d::GRAY16> mask):
_image(image), _mask(mask), _pixelCount(image.GetWidth()*image.GetHeight())
{}


