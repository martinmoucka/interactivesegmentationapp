//
//  RandomWalkSegmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 22.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef RandomWalkSegmenter_hpp
#define RandomWalkSegmenter_hpp

#include <stdio.h>
#include "NaryScribbleSegmenter.hpp"
#include "RandomWalkSegmenterBackend.hpp"
#include <i3d/image3d.h>

// Segmenter implementing Random Walks segmentation technique

class RandomWalkSegmenter : public NaryScribbleSegmenter {
    
protected:
    wxSlider* _sigmaSlider; // Slider to set paramter sigma
public:
    // Constructor
    RandomWalkSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    
    // Overriding methods
    virtual void Init();
    virtual void AddButtonsToRibbon();
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask);
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask);
    
};

#endif /* RandomWalkSegmenter_hpp */
