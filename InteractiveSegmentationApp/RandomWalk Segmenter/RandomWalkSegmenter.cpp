//
//  RandomWalkSegmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 22.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "RandomWalkSegmenter.hpp"

RandomWalkSegmenter::RandomWalkSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame) : NaryScribbleSegmenter(id, ribbonBar, parentFrame) {
    _name = "Random Walk Segmenter";
}

void RandomWalkSegmenter::Init() {
    NaryScribbleSegmenter::Init();
    _width = 2;
}

i3d::Image3d<i3d::GRAY16> RandomWalkSegmenter::RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask) {
    RandomWalkSegmenterBackend<i3d::GRAY8> backend(img, mask);
    backend.SetSigma(_sigmaSlider->GetValue());
    try {
        return backend.RunSegmentation();
    } catch(...){
        return mask;
    }
    
}

i3d::Image3d<i3d::GRAY16> RandomWalkSegmenter::RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask) {
    RandomWalkSegmenterBackend<i3d::RGB> backend(img, mask);
    backend.SetSigma(_sigmaSlider->GetValue());
    try {
        return backend.RunSegmentation();
    } catch(...){
        return mask;
    }
}

void RandomWalkSegmenter::AddButtonsToRibbon(){
    
    wxRibbonPanel *panel = new wxRibbonPanel(_ribbonPage, wxID_ANY, _(_name), wxNullBitmap, wxDefaultPosition, wxDLG_UNIT(_ribbonPage, wxSize(-1,-1)), wxRIBBON_PANEL_DEFAULT_STYLE);
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    panel->SetSizer(sizer);
    
    // Add sigma slider
    wxBoxSizer* sigmaSizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(sigmaSizer, 1, wxALL|wxEXPAND);
    wxStaticText* sigmaStaticText = new wxStaticText(panel, wxID_ANY, wxT("Sigma"));
    sigmaStaticText->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    sigmaSizer->Add(sigmaStaticText, 0, wxALL|wxALIGN_CENTER);
    
    _sigmaSlider = new wxSlider(panel, wxID_ANY, 5, 1, 50, wxDefaultPosition, wxDefaultSize, wxSL_LABELS|wxSL_HORIZONTAL);
    _sigmaSlider->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    sigmaSizer->Add(_sigmaSlider);
    
    NaryScribbleSegmenter::AddButtonsToRibbon();
    
}
