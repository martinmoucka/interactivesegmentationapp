//
//  GrabCutSegmenterBackend.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 27.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef GrabCutSegmenterBackend_hpp
#define GrabCutSegmenterBackend_hpp

#include <stdio.h>
#include "GraphCutSegmenterBackendBase.hpp"
#include <armadillo>
#include "Util.hpp"
#endif /* GrabCutSegmenterBackend_hpp */

using namespace arma;

// Class implementing the computation step of GrabCut segmentation technique
// It should be used followingly:
// 1. Construct object: GrabCutSegmenterBackend(i3d::Image3d<PIXEL> &image)
// 2. Optionally set parameters
//    by calling SetSigma(...), SetLambda(...), SetIterations(...), SetNumComponents(...)
// 3. Set coordinates of GrabCut bounding box by calling SetBoundingBox(...)
// 4. Compute segmentation by calling RunSegmentation()
// 5. If not satisfied, repeat:
//    -- Optionally call SetSigma(...), SetLambda(...)
//    -- Call RunSegmentationWithScribbles(i3d::Image3d<i3d::GRAY16> &scribbles)

template <class PIXEL>
class GrabCutSegmenterBackend : public GraphCutSegmenterBackendBase<PIXEL> {
protected:
    using GraphCutSegmenterBackendBase<PIXEL>::_image;
    using GraphCutSegmenterBackendBase<PIXEL>::_pixelConnectivity;
    // Bounding box mask
    i3d::Image3d<i3d::GRAY16> _bbMask;
    // Bounding box coordinates.
    size_t topLeftX = 0;
    size_t topLeftY = 0;
    size_t bottomRightX = 0;
    size_t bottomRightY = 0;
    // Number of gaussian mixture components
    size_t _k = 5; // Named K in GrabCut paper
    size_t _iterations = 5; // Max number of iterations

    i3d::Image3d<double> fgProbMap;
    i3d::Image3d<double> bgProbMap;
    Gc::Flow::General::Kohli<double, double, double> graph;
public:
    
    // Default constructor
    GrabCutSegmenterBackend() {}
    
    // Constructor
    // 'image' is an original image being segmented.
    GrabCutSegmenterBackend(i3d::Image3d<PIXEL> &image) : GraphCutSegmenterBackendBase<PIXEL>(image) {}
    
    // Used to set maximum number of iterations.
    void SetIterations(double iterations){ _iterations = iterations;}
    // Used to setnumber of GMM components.
    void SetNumComponents(size_t k){ _k = k;}
    // Used to set GrabCut bounding box.
    void SetBoundingBox(size_t topLeftX, size_t topLeftY, size_t bottomRightX, size_t bottomRightY) {
        this->topLeftX = topLeftX;
        this->topLeftY = topLeftY;
        this->bottomRightX = bottomRightX;
        this->bottomRightY = bottomRightY;
        
        // Create mask
        _bbMask.MakeRoom(_image.GetWidth(), _image.GetHeight(), 1);
        _bbMask.SetAllVoxels(BinaryLabelType::Background);
        
        for (size_t x = topLeftX; x <= bottomRightX; ++x) {
            for (size_t y = topLeftY; y <= bottomRightY; ++y) {
                _bbMask.SetVoxel(x, y, 0, BinaryLabelType::Foreground);
            }
        }
    }
    
    // Used to compute segmentation.
    // Returned image contains BinaryLabelType::Foreground and BinaryLabelType::Background
    // to indicate foreground and background regions respectively.
    i3d::Image3d<i3d::GRAY16> RunSegmentation() {
        
        size_t width = _image.GetWidth();
        size_t height = _image.GetHeight();
        std::vector<PIXEL> imgVector;
        imgVector.reserve(width*height);
        for(auto p : _image.GetVoxelData()) { imgVector.push_back(p); }
        mat imgData;
        ConvertVectorDataToMat(imgVector, imgData);
        size_t numChannels = imgData.n_rows;
        i3d::Image3d<i3d::GRAY16> mask = _bbMask;
        i3d::Image3d<i3d::GRAY16> emptyMask;
        emptyMask.MakeRoom(width, height, 1);
        emptyMask.SetAllVoxels(0);
        AddBackgroundToMask(emptyMask);
        
        gmm_full fgModel, bgModel;
        
        
        auto maxNumEdges = ( _pixelConnectivity == GraphCutSegmenterBackendBase<PIXEL>::PixelConnectivity::Four) ? 2*width*height-width-height : 4*width*height - 2*width - 2*height;
        graph.Init(width*height, maxNumEdges > 0 ? maxNumEdges : 1, width*height, width*height);
        this->SetNeighbourCosts(graph);
        
        for (size_t i = 0; i < _iterations; ++i){
            // Create matrices for foreground and background data
            mat fgData, bgData;
            ExtractForegroundAndBackgroundData(fgData, bgData, mask);
            if (fgData.is_empty() || bgData.is_empty()) return mask;
            auto fgNumGaussians = std::min( size(fgData).n_cols, (unsigned long long)_k );
            auto bgNumGaussians = std::min( size(bgData).n_cols, (unsigned long long)_k );
            
            if (i == 0) {
                fgModel.learn(fgData, fgNumGaussians, maha_dist, random_subset, 7, 0, 1e-5, true);
                bgModel.learn(bgData, bgNumGaussians, maha_dist, random_subset, 7, 0, 1e-5, true);
            } else {
                
                // Assign most probable GMM component to each FG/BG pixel
                urowvec fgMostProbableComponents = fgModel.assign(fgData, prob_dist);
                urowvec bgMostProbableComponents = bgModel.assign(bgData, prob_dist);
                // Create structures to store parameters of foreground and background GMMs
                rowvec fgWeights(fgNumGaussians), bgWeights(bgNumGaussians);
                mat fgMeans(numChannels, fgNumGaussians), bgMeans(numChannels, bgNumGaussians);
                cube fgCovs(numChannels, numChannels, fgNumGaussians), bgCovs(numChannels, numChannels, bgNumGaussians);
                // Compute parameters of GMMs
                ComputeGMMParameters(fgData, fgMostProbableComponents, fgWeights, fgMeans, fgCovs);
                ComputeGMMParameters(bgData, bgMostProbableComponents, bgWeights, bgMeans, bgCovs);
                if(sum(fgWeights) == 0 || sum(bgWeights) == 0) {
                    std::cout << "Zero sum of weights" << std::endl;
                    break;
                }
                fgModel.set_params(fgMeans, fgCovs, fgWeights);
                bgModel.set_params(bgMeans, bgCovs, bgWeights);
                
            }
            
            rowvec fgProb = -fgModel.log_p(imgData);
            rowvec bgProb = -bgModel.log_p(imgData);
            
            CreateProbabilityMapFromVector(fgProb, fgProbMap);
            CreateProbabilityMapFromVector(bgProb, bgProbMap);
            
            this->SetTerminalCosts(graph, fgProbMap, bgProbMap, emptyMask);
            graph.FindMaxFlow();
            mask = this->GetMaskFromFlow(graph);
        }
        
        return mask;
    }
    
    // Run segmentation with additional user input.
    // 'scribbles' should contain foreground or/and background scribbles with values
    // BinaryLabelType::Foreground, BinaryLabelType::Background, respectively.
    i3d::Image3d<i3d::GRAY16> RunSegmentationWithScribbles(i3d::Image3d<i3d::GRAY16> &scribbles) {
        size_t width = _image.GetWidth();
        size_t height = _image.GetHeight();
        if (scribbles.GetWidth() != width || scribbles.GetHeight() != height) return _bbMask;
        AddBackgroundToMask(scribbles);
        this->SetTerminalCosts(graph, fgProbMap, bgProbMap, scribbles);
        graph.FindMaxFlow();
        return this->GetMaskFromFlow(graph);
    }
    
protected:
    
    void ExtractForegroundAndBackgroundData(mat &fgData, mat &bgData, i3d::Image3d<i3d::GRAY16> &mask) {
        // mask and image must have equal dimensions
        if (mask.GetWidth() != _image.GetWidth() || mask.GetHeight() != _image.GetHeight()) {
            return;
        }
        size_t pixelCount = _image.GetWidth() * _image.GetHeight();
        std::vector<PIXEL> fgVector, bgVector;
        fgVector.reserve(pixelCount);
        bgVector.reserve(pixelCount);
        
        auto imgIt = _image.begin();
        auto maskIt = mask.begin();
        for (; imgIt != _image.end(); ++imgIt, ++maskIt) {
            
            if (*maskIt == BinaryLabelType::Background) {
                bgVector.push_back(*imgIt);
                continue;
            }
            
            if (*maskIt == BinaryLabelType::Foreground) {
                fgVector.push_back(*imgIt);
                continue;
            }
        }
        
        ConvertVectorDataToMat(fgVector, fgData);
        ConvertVectorDataToMat(bgVector, bgData);
    }
    
    void ConvertVectorDataToMat(std::vector<i3d::GRAY16> &in, mat &out) {
        out = mat(1, in.size());
        size_t i = 0;
        for (auto pixel : in) {
            out(0,i) = pixel;
            ++i;
        }
    }
    
    void ConvertVectorDataToMat(std::vector<i3d::GRAY8> &in, mat &out) {
        out = mat(1, in.size());
        size_t i = 0;
        for (auto pixel : in) {
            out(0,i) = pixel;
            ++i;
        }
    }
    
    void ConvertVectorDataToMat(std::vector<i3d::RGB> &in, mat &out) {
        out = mat(3, in.size());
        size_t i = 0;
        for (auto pixel : in) {
            out(0,i) = pixel.red;
            out(1,i) = pixel.green;
            out(2,i) = pixel.blue;
            ++i;
        }
    }
    
    void CreateProbabilityMapFromVector(rowvec &in, i3d::Image3d<double> &probMap) {
        probMap.MakeRoom(_image.GetWidth(), _image.GetHeight(), 1);
        auto vIt = in.begin();
        auto pIt = probMap.begin();
        while (vIt != in.end()) {
            *pIt = (*vIt);
            ++vIt; ++pIt;
        }
    }
    
    i3d::Image3d<i3d::GRAY16> GetMaskFromFlow(Gc::Flow::IMaxFlow<double, double, double> &graph) {
        i3d::Image3d<i3d::GRAY16> segmented;
        segmented.MakeRoom(this->_image.GetWidth(), this->_image.GetHeight(), 1);
        auto it = segmented.begin();
        size_t i = 0;
        while (it != segmented.end()) {
            
            Gc::Flow::Origin origin = graph.NodeOrigin(i);
            if (origin == Gc::Flow::Origin::Source ) {
                *it = BinaryLabelType::Foreground;
            } else {
                *it = BinaryLabelType::Background;
            }
            
            it++; i++;
        }
        return segmented;
    }
    
    void AddBackgroundToMask(i3d::Image3d<i3d::GRAY16> &mask) {
        auto mit = mask.begin();
        auto bit = _bbMask.begin();
        while (mit != mask.end()) {
            
            if (*bit == BinaryLabelType::Background) {
                *mit = *bit;
            }
            
            mit++;bit++;
        }
    }
    
    void ComputeGMMParameters(const mat &data, const urowvec &assignments, rowvec &weights, mat &means, cube &covs) {
        size_t numChannels = means.n_rows;
        size_t numComponents = means.n_cols;
        for(size_t component = 0; component < numComponents; ++component) {
            // Find indices of pixels that are assigned to current component
            uvec indices = find(assignments == component);

            mat m = mat(numChannels, 1, fill::zeros);
            mat c = mat(numChannels, numChannels, fill::zeros);
            double w = double(indices.n_elem) / data.n_cols;
            // If there are some pixels assigned to current component,
            // compute their mean and covariance matrix.
            if(indices.n_elem > 0) {
                auto componentPixels = data.cols(indices);
                m = mean(componentPixels,1);
                c = cov(componentPixels.t());
                if (c.n_rows != numChannels || c.n_cols != numChannels) {
                    c = mat(numChannels, numChannels, fill::zeros);
                }
            }

            // Add small constant to avoid singular matrix
            c.diag() += 1e-4;

            weights(component) = w;
            means.col(component) = m;
            covs.slice(component) = c;
        }
    }
};
