//
//  GrabCutSegmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 27.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef GrabCutSegmenter_hpp
#define GrabCutSegmenter_hpp

#include <stdio.h>
#include "BinaryScribbleSegmenter.hpp"
#include "GrabCutSegmenterBackend.hpp"

// New mouse mode which adds 'grabCutBBox' mode.
class GrabCutSegmenterMouseMode : public ScribbleSegmenterMouseMode {
    bool _grabCutBBox;
protected:
    virtual void UnsetAll() {_grabCutBBox = false; ScribbleSegmenterMouseMode::UnsetAll();}
public:
    void SetGrabCutBBox() { UnsetAll(); _grabCutBBox = true;}
    bool IsGrabCutBBox() {return _grabCutBBox;}
};

// Class implementing the GrabCut segmentation technique.
class GrabCutSegmenter : public BinaryScribbleSegmenter {
    
protected:
    // Sliders for setting parameters.
    wxSlider* _lambdaSlider;
    wxSlider* _sigmaSlider;
    wxSlider* _gmmSlider;
    wxSlider* _iterationsSlider;
    // Backends for the computation step.
    GrabCutSegmenterBackend<i3d::GRAY8> _backendGray;
    GrabCutSegmenterBackend<i3d::RGB> _backendRGB;
    
    // Internal variables
    // ------------------
    // Says whether initial segmentation was run and user can add additional unput
    bool _initialSegmentationRun = false;
    // GrabCut bounding box.
    wxRect _gbcBbox = wxRect(0,0,0,0);
    
    // Override method for getting active mouse mode.
    virtual GrabCutSegmenterMouseMode* GetMouseMode() {return (GrabCutSegmenterMouseMode*)_mouseMode;}
    
    // Enable to add additional user input in form of scribbles
    void EnableScribbles(bool enable);
    // Toggle GrabCut bounding box tool
    virtual void ToggleGbcBoundingBox();
    // Draw GrabCut bounding box to screen
    void DrawGbcBox(wxDC& dc);
    
public:
    // Constructor
    GrabCutSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    
    // Override methods
    virtual void SetImage(std::shared_ptr<SegmentedImage> image, std::string name);
    virtual void Init();
    virtual void AddButtonsToRibbon();
    virtual void ResetUI();
    virtual void UpdateUI();
    virtual void Render(wxDC& dc);
    virtual void ToggleBoundingBox();
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask);
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask);
    
    
    // Event handling. Override methods.
    virtual void OnMouseLeftDown(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseLeftUp(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseMotion(wxMouseEvent& event, bool insideImage);
    virtual void OnRibbonButtonClicked(wxRibbonButtonBarEvent &event);
    
    // Define new ids for button.
    enum {
        ID_BUTTON_GRABCUT_BOUNDING_BOX = 100001
    };
};

#endif /* GrabCutSegmenter_hpp */
