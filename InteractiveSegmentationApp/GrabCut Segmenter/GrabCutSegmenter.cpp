//
//  GrabCutSegmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 27.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "GrabCutSegmenter.hpp"

GrabCutSegmenter::GrabCutSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame) : BinaryScribbleSegmenter(id, ribbonBar, parentFrame) {
    _name = "GrabCut Segmenter";
    if (_mouseMode != NULL) delete _mouseMode;
    _mouseMode = new GrabCutSegmenterMouseMode;
}

void GrabCutSegmenter::Init() {
    BinaryScribbleSegmenter::Init();
    GetMouseMode()->SetGrabCutBBox();
    UpdateUI();
}

void GrabCutSegmenter::SetImage(std::shared_ptr<SegmentedImage> image, std::string name) {
    BinaryScribbleSegmenter::SetImage(image, name);
    
    if (image->IsGray8()) {
        auto img = _segmentedImage->GetOriginalGrayImage();
        _backendGray = GrabCutSegmenterBackend<i3d::GRAY8>( img );
    } else {
        auto img = _segmentedImage->GetOriginalRgbImage();
        _backendRGB = GrabCutSegmenterBackend<i3d::RGB>( img );
    }
    _bbox = wxRect(0,0,0,0);
    ResetUI();
}

i3d::Image3d<i3d::GRAY16> GrabCutSegmenter::RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask) {

    wxRect bbox = FixRect(_bbox);
    if (bbox.GetWidth() == 0  || bbox.GetHeight() == 0)
        bbox = wxRect(0, 0, mask.GetWidth(), mask.GetHeight());
    
    
    if (!_initialSegmentationRun || mask.GetMaxValue() == 0) {
        _backendGray = GrabCutSegmenterBackend<i3d::GRAY8>( img );
        _backendGray.SetSigma(_sigmaSlider->GetValue());
        _backendGray.SetLambda(_lambdaSlider->GetValue());
        _backendGray.SetNumComponents(_gmmSlider->GetValue());
        _backendGray.SetIterations(_iterationsSlider->GetValue());
        wxRect gbcBox = FixRect(_gbcBbox);
        if (gbcBox.GetWidth() == 0  || gbcBox.GetHeight() == 0 || !bbox.Intersects(gbcBox)) {
            return mask;
        }
        
        
        _backendGray.SetBoundingBox(std::max(0, gbcBox.GetLeft() - bbox.GetLeft()),
                                    std::max(0, gbcBox.GetTop() - bbox.GetTop()),
                                    std::min(bbox.GetWidth()-1, gbcBox.GetRight() - bbox.GetLeft()),
                                    std::min(bbox.GetHeight()-1, gbcBox.GetBottom() - bbox.GetTop()));
    }
    
    if (mask.GetMaxValue() == 0) {
        
        try {
            auto result = _backendGray.RunSegmentation();
            EnableScribbles(true);
            _initialSegmentationRun = true;
            return result;
        } catch(...){
            return mask;
        }
        
    } else {
        // Add scribbles
        _backendGray.SetSigma(_sigmaSlider->GetValue());
        _backendGray.SetLambda(_lambdaSlider->GetValue());
        try {
            return _backendGray.RunSegmentationWithScribbles(mask);
        } catch (...) {
            return mask;
        }
        
    }
}

i3d::Image3d<i3d::GRAY16> GrabCutSegmenter::RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask) {
    
    
    wxRect bbox = FixRect(_bbox);
    if (bbox.GetWidth() == 0  || bbox.GetHeight() == 0)
        bbox = wxRect(0, 0, mask.GetWidth(), mask.GetHeight());
    
    if (!_initialSegmentationRun || mask.GetMaxValue() == 0) {
        _backendRGB = GrabCutSegmenterBackend<i3d::RGB>( img );
        _backendRGB.SetSigma(_sigmaSlider->GetValue());
        _backendRGB.SetLambda(_lambdaSlider->GetValue());
        _backendRGB.SetNumComponents(_gmmSlider->GetValue());
        _backendRGB.SetIterations(_iterationsSlider->GetValue());
        wxRect gbcBox = FixRect(_gbcBbox);
        if (gbcBox.GetWidth() == 0  || gbcBox.GetHeight() == 0 || !bbox.Intersects(gbcBox)) {
            return mask;
        }
        
        _backendRGB.SetBoundingBox(std::max(0, gbcBox.GetLeft() - bbox.GetLeft()),
                                   std::max(0, gbcBox.GetTop() - bbox.GetTop()),
                                   std::min(bbox.GetWidth()-1, gbcBox.GetRight() - bbox.GetLeft()),
                                   std::min(bbox.GetHeight()-1, gbcBox.GetBottom() - bbox.GetTop()));
    }
    if (mask.GetMaxValue() == 0) {
        
        try {
            auto result = _backendRGB.RunSegmentation();
            EnableScribbles(true);
            _initialSegmentationRun = true;
            return result;
        } catch(...){
            return mask;
        }
        
    } else {
        // Add scribbles
        _backendRGB.SetSigma(_sigmaSlider->GetValue());
        _backendRGB.SetLambda(_lambdaSlider->GetValue());
        try {
            return _backendRGB.RunSegmentationWithScribbles(mask);
        } catch (...) {
            return mask;
        }
        
    }
}

void GrabCutSegmenter::OnRibbonButtonClicked(wxRibbonButtonBarEvent &event)
{
    int id = _ribbonButtonBar->GetItemId(event.GetButton());
    switch (id) {
        case ID_BUTTON_GRABCUT_BOUNDING_BOX:
            ToggleGbcBoundingBox();
            break;
        default:
            BinaryScribbleSegmenter::OnRibbonButtonClicked(event);
            break;
    }
}

void GrabCutSegmenter::ResetUI() {
    EnableScribbles(false);
    _gbcBbox = wxRect(0,0,0,0);
    GetMouseMode()->SetGrabCutBBox();
    _initialSegmentationRun = false;
    RefreshCanvas();
    UpdateUI();
}

void GrabCutSegmenter::UpdateUI() {
    if (GetMouseMode()->IsGrabCutBBox()) {
        _ribbonButtonBar->ToggleButton(ID_BUTTON_GRABCUT_BOUNDING_BOX, true);
        _ribbonButtonBar->ToggleButton(ID_BUTTON_BACKGROUND_LABEL, false);
        _ribbonButtonBar->ToggleButton(ID_BUTTON_FOREGROUND_LABEL, false);
        _ribbonButtonBar->ToggleButton(ID_BUTTON_BOUNDING_BOX, false);
    } else {
        _ribbonButtonBar->ToggleButton(ID_BUTTON_GRABCUT_BOUNDING_BOX, false);
        BinaryScribbleSegmenter::UpdateUI();
    }
}

void GrabCutSegmenter::EnableScribbles(bool enable) {
    _ribbonButtonBar->EnableButton(BinaryScribbleSegmenter::ID_BUTTON_BACKGROUND_LABEL, enable);
    _ribbonButtonBar->EnableButton(BinaryScribbleSegmenter::ID_BUTTON_FOREGROUND_LABEL, enable);
}

void GrabCutSegmenter::ToggleBoundingBox() {

    if (GetMouseMode()->IsBoundingBox()) {
        GetMouseMode()->SetNone();
    } else {
        GetMouseMode()->SetBoundingBox();
    }
    UpdateUI();
}

void GrabCutSegmenter::ToggleGbcBoundingBox() {

    if (GetMouseMode()->IsGrabCutBBox()) {
        GetMouseMode()->SetNone();
    } else {
        GetMouseMode()->SetGrabCutBBox();
    }
    UpdateUI();
}

void GrabCutSegmenter::Render(wxDC &dc) {
    Segmenter::Render(dc);
    DrawBox(dc);
    DrawGbcBox(dc);
    if (GetMouseMode()->IsScribble() && _drawCursor) DrawCursor(dc);
}

void GrabCutSegmenter::DrawGbcBox(wxDC& dc) {
    
    if (_gbcBbox.GetWidth() == 0 || _gbcBbox.GetHeight() == 0) {
        return;
    }
    wxGraphicsContext *gc = wxGraphicsContext::CreateFromUnknownDC(dc);
    gc->SetPen(wxPen(wxColour(255, 220, 50, 255), 3, wxPENSTYLE_SOLID));
    gc->SetBrush(wxBrush(wxColour(0, 0, 0, 0)));
    gc->DrawRectangle(_gbcBbox.GetX(), _gbcBbox.GetY(), _gbcBbox.GetWidth(), _gbcBbox.GetHeight());
    delete gc;
}

// MARK: - Mouse event handling

void GrabCutSegmenter::OnMouseLeftDown(wxMouseEvent &event, bool insideImage)
{
    if (GetMouseMode()->IsGrabCutBBox()) {
        _gbcBbox = wxRect(event.GetPosition(), event.GetPosition());
        _initialSegmentationRun = false;
        EnableScribbles(false);
        ClearMarkersBitmap();
        RefreshCanvas();
    } else {
        if (GetMouseMode()->IsBoundingBox()) {
            _initialSegmentationRun = false;
            EnableScribbles(false);
            ClearMarkersBitmap();
        }
    }
    
    BinaryScribbleSegmenter::OnMouseLeftDown(event, insideImage);
}

void GrabCutSegmenter::OnMouseLeftUp(wxMouseEvent &event, bool insideImage)
{
    BinaryScribbleSegmenter::OnMouseLeftUp(event, insideImage);
}

void GrabCutSegmenter::OnMouseMotion(wxMouseEvent &event, bool insideImage)
{
    if (GetMouseMode()->IsGrabCutBBox() && insideImage && event.LeftIsDown()) {
        _gbcBbox.SetBottomRight(event.GetPosition());
    }
    BinaryScribbleSegmenter::OnMouseMotion(event, insideImage);
}

void GrabCutSegmenter::AddButtonsToRibbon(){
    
    _ribbonButtonBar->AddButton(ID_BUTTON_GRABCUT_BOUNDING_BOX, _("GrabCut Bounding box"), wxArtProvider::GetBitmap(wxART_CUT, wxART_BUTTON, wxDefaultSize), _("GrabCut Bounding box"), wxRIBBON_BUTTON_TOGGLE);
    
    //BinaryScribbleSegmenter::AddButtonsToRibbon();
    wxRibbonPanel *panel = new wxRibbonPanel(_ribbonPage, wxID_ANY, _(_name), wxNullBitmap, wxDefaultPosition, wxDLG_UNIT(_ribbonPage, wxSize(-1,-1)), wxRIBBON_PANEL_DEFAULT_STYLE);
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    panel->SetSizer(sizer);
    
    // Add lambda slider
    wxBoxSizer* lambdaSizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(lambdaSizer, 1, wxALL|wxEXPAND);
    wxStaticText* lambdaStaticText = new wxStaticText(panel, wxID_ANY, wxT("Lambda"));
    lambdaStaticText->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    lambdaSizer->Add(lambdaStaticText, 0, wxALL|wxALIGN_CENTER);
    
    _lambdaSlider = new wxSlider(panel, wxID_ANY, 60, 0, 100, wxDefaultPosition, wxSize(100,30), wxSL_LABELS|wxSL_HORIZONTAL);
    _lambdaSlider->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    lambdaSizer->Add(_lambdaSlider);
    
    // Add sigma slider
    wxBoxSizer* sigmaSizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(sigmaSizer, 1, wxALL|wxEXPAND);
    wxStaticText* sigmaStaticText = new wxStaticText(panel, wxID_ANY, wxT("Sigma"));
    sigmaStaticText->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    sigmaSizer->Add(sigmaStaticText, 0, wxALL|wxALIGN_CENTER);
    
    _sigmaSlider = new wxSlider(panel, wxID_ANY, 20, 1, 50, wxDefaultPosition,  wxSize(100,30), wxSL_LABELS|wxSL_HORIZONTAL);
    _sigmaSlider->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    sigmaSizer->Add(_sigmaSlider);
    
    // Add gmm slider
    wxBoxSizer* gmmSizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(gmmSizer, 1, wxALL|wxEXPAND);
    wxStaticText* gmmStaticText = new wxStaticText(panel, wxID_ANY, wxT("GMM components"));
    gmmStaticText->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    gmmSizer->Add(gmmStaticText, 0, wxALL|wxALIGN_CENTER);
    
    _gmmSlider = new wxSlider(panel, wxID_ANY, 5, 1, 20, wxDefaultPosition,  wxSize(100,30), wxSL_LABELS|wxSL_HORIZONTAL);
    _gmmSlider->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    gmmSizer->Add(_gmmSlider);
    
    // Add iter slider
    wxBoxSizer* iterSizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(iterSizer, 1, wxALL|wxEXPAND);
    wxStaticText* iterStaticText = new wxStaticText(panel, wxID_ANY, wxT("Iterations"));
    iterStaticText->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    iterSizer->Add(iterStaticText, 0, wxALL|wxALIGN_CENTER);
    
    _iterationsSlider = new wxSlider(panel, wxID_ANY, 5, 0, 60, wxDefaultPosition, wxSize(100,30), wxSL_LABELS|wxSL_HORIZONTAL);
    _iterationsSlider->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    iterSizer->Add(_iterationsSlider);
    
    
    BinaryScribbleSegmenter::AddButtonsToRibbon();
    
}
