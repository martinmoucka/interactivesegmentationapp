//
//  ManualSegmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 16.04.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "ManualSegmenter.hpp"

ManualSegmenter::ManualSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame) : NaryScribbleSegmenter(id, ribbonBar, parentFrame) {
    _name = "Manual Segmenter";
    _showBoundingBoxButton = false;
    _showRunSegmentationButton = false;
    _showClearSegmentationResultButton = false;
    _showAddToSegmentationButton = false;
    _showClearMarkersButton = false;
}

void ManualSegmenter::Init() {
    NaryScribbleSegmenter::Init();
    _width = 1;
    UpdateUI();
}

void ManualSegmenter::SetBrushMode(BrushMode mode){
    _brushMode = mode;
    UpdateUI();
}

void ManualSegmenter::AddButtonsToRibbon() {

    _ribbonButtonBar->AddButton(ID_BUTTON_STROKE, _("Stroke area"), wxArtProvider::GetBitmap(wxART_TIP, wxART_BUTTON, wxDefaultSize), _("Stroke area"), wxRIBBON_BUTTON_TOGGLE);
    
    _ribbonButtonBar->AddButton(ID_BUTTON_FILL, _("Fill area"), wxArtProvider::GetBitmap(wxART_TIP, wxART_BUTTON, wxDefaultSize), _("Fill area"), wxRIBBON_BUTTON_TOGGLE);
    
     NaryScribbleSegmenter::AddButtonsToRibbon();
}

// MARK: - Mouse event handling

void ManualSegmenter::OnMouseLeftDown(wxMouseEvent &event, bool insideImage)
{
    NaryScribbleSegmenter::OnMouseLeftDown(event, insideImage);

    if (_brushMode == Fill) {
        currentFillPoints.push_back(event.GetPosition());
    }
}

void ManualSegmenter::OnMouseLeftUp(wxMouseEvent &event, bool insideImage)
{
    NaryScribbleSegmenter::OnMouseLeftUp(event, insideImage);

    if (_brushMode == Fill) {
        DrawFilledArea();
        currentFillPoints.clear();
        RefreshCanvas();
    }
    AddMarkersToSegmentedImage();
    ClearMarkersBitmap();
    
}

void ManualSegmenter::OnMouseMotion(wxMouseEvent &event, bool insideImage)
{
    if (_brushMode == Fill && event.LeftIsDown()) {
        currentFillPoints.push_back(event.GetPosition());
    }
    
    NaryScribbleSegmenter::OnMouseMotion(event, insideImage);
}

void ManualSegmenter::OnRibbonButtonClicked(wxRibbonButtonBarEvent &event)
{
    int id = _ribbonButtonBar->GetItemId(event.GetButton());
    switch (id) {
        case ID_BUTTON_STROKE:
            if (_brushMode == Stroke) {
                SetBrushMode(Fill);
            } else {
                SetBrushMode(Stroke);
            }
            break;
        case ID_BUTTON_FILL:
            if (_brushMode == Stroke) {
                SetBrushMode(Fill);
            } else {
                SetBrushMode(Stroke);
            }
            break;
        default:
            NaryScribbleSegmenter::OnRibbonButtonClicked(event);
            break;
    }
}

void ManualSegmenter::DrawFilledArea() {
    
    // If there are only two or less points, don't draw filled area,
    // because there is nothing to fill.
    if (currentFillPoints.size() <= 2) {
        return;
    }
    
    // Draw to label bitmap
    i3d::GRAY16 label = _drawingLabel;
    if (label == 0) label = std::numeric_limits<i3d::GRAY16>::max();
    wxGraphicsContext *lgc = wxGraphicsContext::Create(_markersBitmap);
    lgc->SetAntialiasMode(wxANTIALIAS_NONE);
    wxColour iColor = Segmenter::ConvertGRAY16LabelToInternalwxColor(label);
    lgc->SetPen( wxPen( iColor , _width, wxPENSTYLE_SOLID) );
    lgc->SetBrush(wxBrush(iColor));
    lgc->DrawLines(currentFillPoints.size(), &currentFillPoints[0], wxWINDING_RULE);
    delete lgc;
    
    // Draw to bitmap that is displayed
    wxGraphicsContext *dgc = wxGraphicsContext::Create(_markersDisplayBitmap);
    dgc->SetAntialiasMode(wxANTIALIAS_NONE);
    wxColour dColor = RGBToWxColour( ColorFromLabel(_drawingLabel) );
    dgc->SetPen( wxPen( dColor , _width, wxPENSTYLE_SOLID) );
    dgc->SetBrush( wxBrush(dColor) );
    dgc->DrawLines(currentFillPoints.size(), &currentFillPoints[0], wxWINDING_RULE);
    delete dgc;
}

void ManualSegmenter::AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels) {
    AddMarkersToSegmentedImage();
}

void ManualSegmenter::AddMarkersToSegmentedImage() {
    auto markers = Segmenter::ConvertLabelWxImageToImage3dGRAY16(_markersBitmap.ConvertToImage());
    _segmentedImage->AddLabels(markers);
}

void ManualSegmenter::UpdateUI() {
    NaryScribbleSegmenter::UpdateUI();
    _ribbonButtonBar->ToggleButton(ID_BUTTON_STROKE, _brushMode == Stroke);
    _ribbonButtonBar->ToggleButton(ID_BUTTON_FILL, _brushMode == Fill);
}
