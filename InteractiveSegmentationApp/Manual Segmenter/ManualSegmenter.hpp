//
//  ManualSegmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 16.04.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef ManualSegmenter_hpp
#define ManualSegmenter_hpp

#include <stdio.h>
#include "NaryScribbleSegmenter.hpp"
#include <i3d/image3d.h>

// Class implementing manual segmentation

class ManualSegmenter : public NaryScribbleSegmenter {
    
    // Brush mode.
    // Used to determine whether we should fill the stroked area.
    enum BrushMode{ Stroke, Fill };
    BrushMode _brushMode = Stroke;
    void SetBrushMode(BrushMode mode);
    
    //Internal variables
    // Used to store points of polygon that should be filled.
    std::vector<wxPoint2DDouble> currentFillPoints;
    
public:
    // Constructor
    ManualSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    virtual void Init();
    
    // Override methods
    virtual void AddButtonsToRibbon();
    virtual void UpdateUI();
    virtual void OnRibbonButtonClicked(wxRibbonButtonBarEvent& event);
    virtual void AddSegmentationResultToSegmentedImage(i3d::Image3d<i3d::GRAY16> &labels);
    
    // Used to add markers to segmentation results directly.
    void AddMarkersToSegmentedImage();
    // Used to draw filled area to the '_markersBitmap' and to '_markersDisplayBitmap'.
    virtual void DrawFilledArea();
    
    // Mouse events handling
    virtual void OnMouseLeftDown(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseLeftUp(wxMouseEvent& event, bool insideImage);
    virtual void OnMouseMotion(wxMouseEvent& event, bool insideImage);
    
    // Define new ids for buttons.
    enum {
        ID_BUTTON_STROKE = 100001,
        ID_BUTTON_FILL = 100002,
    };

};



#endif /* ManualSegmenter_hpp */
