//
//  GraphCutSegmenter.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 12.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef GraphCutSegmenter_hpp
#define GraphCutSegmenter_hpp

#include <stdio.h>
#include <wx/slider.h>
#include "BinaryScribbleSegmenter.hpp"
#include "GraphCutSegmenterBackend.hpp"

// Segmenter implementing the Interactive Graph Cuts technique

class GraphCutSegmenter : public BinaryScribbleSegmenter {
    
protected:
    // Sliders for setting parameters
    wxSlider* _lambdaSlider;
    wxSlider* _sigmaSlider;
    
public:
    // Constructor
    GraphCutSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame);
    
    // Overwriting methods
    virtual void Init();
    virtual void AddButtonsToRibbon();
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask);
    virtual i3d::Image3d<i3d::GRAY16> RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask);
};

#endif /* GraphCutSegmenter_hpp */
