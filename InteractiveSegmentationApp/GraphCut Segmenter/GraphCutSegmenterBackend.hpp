//
//  GraphCutSegmenterBackend.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 07.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef GraphCutSegmenterBackend_hpp
#define GraphCutSegmenterBackend_hpp

#include <stdio.h>
#include <i3d/histogram.h>
#include <valarray>
#include "GraphCutSegmenterBackendBase.hpp"

// Class implementing the computation step of the Interactive Graph Cuts technique.
// It should be used followingly:
// 1. Construct object: GraphCutSegmenterBackend(...)
// 2. Optionally set parameters by calling SetLambda(...), SetSigma(...)
// 3. Compute segmentation by calling RunSegmentation()

template <class PIXEL>
class GraphCutSegmenterBackend : public GraphCutSegmenterBackendBase<PIXEL> {

protected:
    // Mask with background/foreground scribbles
    i3d::Image3d<i3d::GRAY16> _mask;

public:
    
    // Constructor
    GraphCutSegmenterBackend(i3d::Image3d<PIXEL> &image, i3d::Image3d<i3d::GRAY16> mask): GraphCutSegmenterBackendBase<PIXEL>(image), _mask(mask)
    {}
    
    i3d::Image3d<i3d::GRAY16> RunSegmentation() {
        size_t width = this->_image.GetWidth();
        size_t height = this->_image.GetHeight();
        
        // Compute likelihoods of each pixel for belonging to BG or FG.
        i3d::Image3d<double> fgProbMap, bgProbMap;
        ComputeProbabilityMaps(this->_image, fgProbMap, bgProbMap);
        
        // Construct the graph
        Gc::Flow::General::Kohli<double, double, double> graph;
        auto maxNumEdges = ( GraphCutSegmenterBackendBase<PIXEL>::_pixelConnectivity == GraphCutSegmenterBackendBase<PIXEL>::PixelConnectivity::Four) ? 2*width*height-width-height : 4*width*height - 2*width - 2*height;
        graph.Init(width*height, maxNumEdges > 0 ? maxNumEdges : 1, width*height, width*height);
        // Set n-links
        this->SetNeighbourCosts(graph);
        // Set t-links.
        this->SetTerminalCosts(graph, fgProbMap, bgProbMap, _mask);
        // Find cut.
        graph.FindMaxFlow();
        // Extract result from cut.
        i3d::Image3d<i3d::GRAY16> segmentation = this->GetSegmentationFromFlow(graph);
        // Return result.
        return segmentation;
    }
    
    // Compute probabilities of image pixels belonging to background/foreground
    void ComputeProbabilityMaps(i3d::Image3d<i3d::GRAY8> &image, i3d::Image3d<double> &fgProbMap, i3d::Image3d<double> &bgProbMap) {

        i3d::Histogram fgHist(256);
        i3d::Histogram bgHist(256);
        unsigned int fgCount = 0, bgCount = 0;
        
        auto imgIt = image.begin();
        auto maskIt = _mask.begin();
        for (; imgIt != image.end(); ++imgIt, ++maskIt) {
            
            if (*maskIt == BinaryLabelType::Background) {
                bgHist[*imgIt] += 1;
                bgCount++;
                continue;
            }
            
            if (*maskIt == BinaryLabelType::Foreground) {
                fgHist[*imgIt] += 1;
                fgCount++;
                continue;
            }
            
        }
        
        i3d::Histogram fgHistSmooth(256);
        i3d::Histogram bgHistSmooth(256);
        i3d::SmoothHist(fgHist, fgHistSmooth);
        i3d::SmoothHist(bgHist, bgHistSmooth);
        std::valarray<double> Pf(256); // Prob. dist foreground
        std::valarray<double> Pb(256); // Prob dist. background
        // Normalize histograms
        double sumF = fgHistSmooth.sum(), sumB = bgHistSmooth.sum();
        for (size_t i = 0; i < 256; ++i) {
            Pf[i] = (double)fgHistSmooth[i] /  sumF;
            Pb[i] = (double)bgHistSmooth[i] /  sumB;
        }
        
        // Create probability maps
        
        fgProbMap.MakeRoom(image.GetWidth(), image.GetHeight(), 1);
        bgProbMap.MakeRoom(image.GetWidth(), image.GetHeight(), 1);
        
        auto it = image.begin();
        auto bgIt = bgProbMap.begin(), fgIt = fgProbMap.begin();
        double maxB = 0, maxF = 0;
        while (it != image.end() ) {
            double PrF = Pf[*it]; // Probability that pixel is in foreground
            double PrB = Pb[*it]; // Probability that pixel is in background
            
            *fgIt = -std::log( -PrF );
            *bgIt = -std::log( -PrB );
            
            if (*fgIt > maxF) maxF = *fgIt;
            if (*bgIt > maxB) maxB = *bgIt;
            
            it++; bgIt++; fgIt++;
        }
        
    }
    
    // Compute probabilities of image pixels belonging to background/foreground
    void ComputeProbabilityMaps(i3d::Image3d<i3d::RGB> &image, i3d::Image3d<double> &fgProbMap, i3d::Image3d<double> &bgProbMap) {
        
        unsigned int fgHist[3][256] = {0};
        unsigned int bgHist[3][256] = {0};
        unsigned int fgCount = 0, bgCount = 0;
        auto imgIt = image.begin();
        auto maskIt = _mask.begin();
        for (; imgIt != image.end(); ++imgIt, ++maskIt) {
            
            if (*maskIt == BinaryLabelType::Background) {
                bgHist[0][imgIt->red] += 1;
                bgHist[1][imgIt->green] += 1;
                bgHist[2][imgIt->blue] += 1;
                bgCount++;
                continue;
            }
            
            if (*maskIt == BinaryLabelType::Foreground) {
                fgHist[0][imgIt->red] += 1;
                fgHist[1][imgIt->green] += 1;
                fgHist[2][imgIt->blue] += 1;
                fgCount++;
                continue;
            }
            
        }
        
        // Compute means
        i3d::RGB_generic<double> fgMean(0.0, 0.0, 0.0);
        i3d::RGB_generic<double> bgMean(0.0, 0.0, 0.0);
        
        for (int i = 0; i < 256; ++i) {
            fgMean.red = fgMean.red + fgHist[0][i]*i;
            fgMean.green = fgMean.green + fgHist[1][i]*i;
            fgMean.blue = fgMean.blue + fgHist[2][i]*i;
            bgMean.red = bgMean.red + bgHist[0][i]*i;
            bgMean.green = bgMean.green + bgHist[1][i]*i;
            bgMean.blue = bgMean.blue + bgHist[2][i]*i;
        }
        
        fgMean.red = fgMean.red/fgCount;
        fgMean.green = fgMean.green/fgCount;
        fgMean.blue = fgMean.blue/fgCount;
        bgMean.red = bgMean.red/bgCount;
        bgMean.green = bgMean.green/bgCount;
        bgMean.blue = bgMean.blue/bgCount;
        
        // Create probability maps
        
        fgProbMap.MakeRoom(image.GetWidth(), image.GetHeight(), 1);
        bgProbMap.MakeRoom(image.GetWidth(), image.GetHeight(), 1);
        
        auto it = image.begin();
        auto bgIt = bgProbMap.begin(), fgIt = fgProbMap.begin();
        double maxB = 0, maxF = 0;
        
        while (it != image.end() ) {
            double fgDist = IntensityDistance(fgMean, *it);
            double bgDist = IntensityDistance(bgMean, *it);
            double div = fgDist + bgDist;
            *fgIt = ( fgDist / div );
            *bgIt = ( bgDist / div );
            
            if (*fgIt > maxF) maxF = *fgIt;
            if (*bgIt > maxB) maxB = *bgIt;
            
            it++; bgIt++; fgIt++;
        }
    }
};

#endif /* GraphCutSegmenterBackend_hpp */
