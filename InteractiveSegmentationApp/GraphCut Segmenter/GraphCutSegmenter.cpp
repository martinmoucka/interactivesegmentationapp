//
//  GraphCutSegmenter.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 12.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "GraphCutSegmenter.hpp"

GraphCutSegmenter::GraphCutSegmenter(int id, wxRibbonBar* ribbonBar, MainFrame* parentFrame) : BinaryScribbleSegmenter(id, ribbonBar, parentFrame) {
    _name = "GraphCut Segmenter";
}

void GraphCutSegmenter::Init() {
    BinaryScribbleSegmenter::Init();
    UpdateUI();
}

i3d::Image3d<i3d::GRAY16> GraphCutSegmenter::RunSegmentation(i3d::Image3d<i3d::GRAY8> img, i3d::Image3d<i3d::GRAY16> mask) {
    GraphCutSegmenterBackend<i3d::GRAY8> backend(img, mask);
    backend.SetLambda(_lambdaSlider->GetValue());
    backend.SetSigma(_sigmaSlider->GetValue());
    return backend.RunSegmentation();
}

i3d::Image3d<i3d::GRAY16> GraphCutSegmenter::RunSegmentation(i3d::Image3d<i3d::RGB> img, i3d::Image3d<i3d::GRAY16> mask) {
    GraphCutSegmenterBackend<i3d::RGB> backend(img, mask);
    backend.SetLambda(_lambdaSlider->GetValue());
    backend.SetSigma(_sigmaSlider->GetValue());
    return backend.RunSegmentation();
}


void GraphCutSegmenter::AddButtonsToRibbon(){
    
    wxRibbonPanel *panel = new wxRibbonPanel(_ribbonPage, wxID_ANY, _(_name), wxNullBitmap, wxDefaultPosition, wxDLG_UNIT(_ribbonPage, wxSize(-1,-1)), wxRIBBON_PANEL_DEFAULT_STYLE);
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    panel->SetSizer(sizer);
    
    // Add lambda slider
    wxBoxSizer* lambdaSizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(lambdaSizer, 1, wxALL|wxEXPAND);
    wxStaticText* lambdaStaticText = new wxStaticText(panel, wxID_ANY, wxT("Lambda"));
    lambdaStaticText->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    lambdaSizer->Add(lambdaStaticText, 0, wxALL|wxALIGN_CENTER);
    
    _lambdaSlider = new wxSlider(panel, wxID_ANY, 20, 0, 50, wxDefaultPosition, wxDefaultSize, wxSL_LABELS|wxSL_HORIZONTAL);
    _lambdaSlider->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    lambdaSizer->Add(_lambdaSlider);
    
    // Add sigma slider
    wxBoxSizer* sigmaSizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(sigmaSizer, 1, wxALL|wxEXPAND);
    wxStaticText* sigmaStaticText = new wxStaticText(panel, wxID_ANY, wxT("Sigma"));
    sigmaStaticText->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    sigmaSizer->Add(sigmaStaticText, 0, wxALL|wxALIGN_CENTER);
    
    _sigmaSlider = new wxSlider(panel, wxID_ANY, 20, 1, 50, wxDefaultPosition, wxDefaultSize, wxSL_LABELS|wxSL_HORIZONTAL);
    _sigmaSlider->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNTEXT));
    sigmaSizer->Add(_sigmaSlider);
    
    BinaryScribbleSegmenter::AddButtonsToRibbon();
    
}
