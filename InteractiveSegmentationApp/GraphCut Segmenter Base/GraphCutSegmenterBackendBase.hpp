//
//  GraphCutSegmenterBackendBase.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 27.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef GraphCutSegmenterBackendBase_hpp
#define GraphCutSegmenterBackendBase_hpp

#include <stdio.h>
#include <i3d/image3d.h>
#include <Gc/Core.h>
#include <Gc/Flow/General/Kohli.h>
#include "Util.hpp"

// Base class for graph-cuts based segmentation techniques.

template <class PIXEL>
class GraphCutSegmenterBackendBase {
public:
    enum PixelConnectivity {Four,Eight};
    
protected:
    i3d::Image3d<PIXEL> _image;
    PixelConnectivity _pixelConnectivity = Eight;
    double _sigma = 2;
    double _lambda = 2;
    double _maxNeighborDegree = 100000; // named 'K' in GraphCut paper.
    
public:
    // Default constructor
    GraphCutSegmenterBackendBase() {}
    // Constructor
    GraphCutSegmenterBackendBase(i3d::Image3d<PIXEL> image) : _image(image) {};
    // Used to set parameters.
    void SetSigma(double sigma){ _sigma = sigma;}
    void SetLambda(double lambda){ _lambda = lambda;}
    void SetPixelConnectivity(PixelConnectivity connectivity){_pixelConnectivity = connectivity;}
    
protected:
    
    // Compute n-link weight from given difference of neighboring pixels.
    double ComputeNeighbourCapacity(double d) {
        double res = _lambda*std::exp( -( (d*d)/(2.0*_sigma*_sigma) ) );
        return res;
    }
    
    // Used to set n-link weights on on 'graph'
    void SetNeighbourCosts(Gc::Flow::IMaxFlow<double, double, double> &graph) {
        
        size_t w = _image.GetWidth();
        size_t h = _image.GetHeight();
        std::vector<double> sumNeighborCosts(w*h, 0);
        auto it = _image.begin();
        size_t i = 0;
        size_t lastRowStart = w*h - w;
        while (it != _image.end()) {
            
            if (i%w == w - 1 /*Last column*/) {
                if ( i < lastRowStart /* Not last row*/) {
                    double dy = IntensityDistance(*it, *(it+w));
                    double cap = ComputeNeighbourCapacity(dy);
                    graph.SetArcCap(i, i+w, cap, cap);
                    sumNeighborCosts.at(i) = sumNeighborCosts.at(i) + cap;
                    sumNeighborCosts.at(i+w) = sumNeighborCosts.at(i+w) + cap;
                }
            } else if ( i >= lastRowStart /*Last row*/) {
                double dx = IntensityDistance(*it, *(it+1));
                double cap = ComputeNeighbourCapacity(dx);
                graph.SetArcCap(i, i+1, cap, cap);
                sumNeighborCosts.at(i) = sumNeighborCosts.at(i) + cap;
                sumNeighborCosts.at(i+1) = sumNeighborCosts.at(i+1) + cap;
            } else {
                double dx = IntensityDistance(*it, *(it+1));
                double dy = IntensityDistance(*it, *(it+w));
                double capX = ComputeNeighbourCapacity(dx);
                double capY = ComputeNeighbourCapacity(dy);
                graph.SetArcCap(i, i+1, capX, capX);
                graph.SetArcCap(i, i+w, capY, capY);
                sumNeighborCosts.at(i) = sumNeighborCosts.at(i) + capX;
                sumNeighborCosts.at(i+1) = sumNeighborCosts.at(i+1) + capX;
                sumNeighborCosts.at(i) = sumNeighborCosts.at(i) + capY;
                sumNeighborCosts.at(i+w) = sumNeighborCosts.at(i+w) + capY;
            }
            if(_pixelConnectivity == Eight) {
                // If not last column && not last row
                if(i%w != w - 1 && i < lastRowStart){
                    double dc = IntensityDistance(*it, *(it+w+1));
                    double cap = ComputeNeighbourCapacity(dc) * 0.707107; // 0.707107 = 1/sqrt(2)
                    graph.SetArcCap(i, i+w+1, cap, cap);
                    sumNeighborCosts.at(i) = sumNeighborCosts.at(i) + cap;
                    sumNeighborCosts.at(i+w+1) = sumNeighborCosts.at(i+w+1) + cap;
                }
                // If not first column && not last row
                if(i%w != 0 && i < lastRowStart){
                    double dc = IntensityDistance(*it, *(it+w-1));
                    double cap = ComputeNeighbourCapacity(dc) * 0.707107; // 0.707107 = 1/sqrt(2)
                    graph.SetArcCap(i, i+w-1, cap, cap);
                    sumNeighborCosts.at(i) = sumNeighborCosts.at(i) + cap;
                    sumNeighborCosts.at(i+w-1) = sumNeighborCosts.at(i+w-1) + cap;
                }
            }
            
            it++; i++;
        }
        _maxNeighborDegree = 1 + *std::max_element(std::begin(sumNeighborCosts), std::end(sumNeighborCosts));
    }
    
    // Used to set t-link weights on 'graph'.
    // 'fgProbMap' and 'bgProbMap' should contain likelihoods of each pixel
    // for belonging to foreground and background respectively.
    // 'mask should contain' BinaryLabelType::Foreground and BinaryLabelType::Background
    // values to indicate foreground and background markers, respectively.
    void SetTerminalCosts(Gc::Flow::IMaxFlow<double, double, double> &graph, i3d::Image3d<double> &fgProbMap, i3d::Image3d<double> &bgProbMap, i3d::Image3d<i3d::GRAY16> mask) {
        auto fgIt = fgProbMap.begin();
        auto bgIt = bgProbMap.begin();
        auto mIt = mask.begin();
        size_t i = 0;
        while (fgIt != fgProbMap.end()) {
            
            double s = 0;
            double t = 0;
            if(*mIt == BinaryLabelType::Foreground){
                s = _maxNeighborDegree;
            }
            else if(*mIt == BinaryLabelType::Background){
                t = _maxNeighborDegree;
            }
            else{
                s = (*bgIt);
                t = (*fgIt);
            }

            graph.SetTerminalArcCap(i, s, t);
            
            fgIt++; bgIt++ ;mIt++; i++;
        }
    }
    
    // Used to extract segmentation result from 'graph',
    // after algorithm for finding max. flow was performed on it.
    // Returns image with BinaryLabelType::Foreground and BinaryLabelType::Background
    // values to indicate foreground and background regions respectively.
    i3d::Image3d<i3d::GRAY16> GetSegmentationFromFlow(Gc::Flow::IMaxFlow<double, double, double> &graph) {
        i3d::Image3d<i3d::GRAY16> segmented;
        segmented.MakeRoom(this->_image.GetWidth(), this->_image.GetHeight(), 1);
        auto it = segmented.begin();
        size_t i = 0;
        while (it != segmented.end()) {
            
            Gc::Flow::Origin origin = graph.NodeOrigin(i);
            if (origin == Gc::Flow::Origin::Source ) {
                *it = BinaryLabelType::Foreground;
            } else {
                *it = BinaryLabelType::Background;
            }
            
            it++; i++;
        }
        return segmented;
    }
    
};

#endif /* GraphCutSegmenterBackendBase_hpp */
