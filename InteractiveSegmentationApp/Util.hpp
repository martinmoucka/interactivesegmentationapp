//
//  Util.hpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 09.02.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#ifndef Util_hpp
#define Util_hpp

#include <stdio.h>
#include <i3d/image3d.h>
#include <wx/wx.h>
#include <chrono>

enum BinaryLabelType {
    Background = 1,
    Foreground = 2
};

double IntensityDistance(i3d::GRAY8 a, i3d::GRAY8 b);

double IntensityDistance(i3d::RGB aa, i3d::RGB bb);

double IntensityDistance(i3d::RGB_generic<double> a, i3d::RGB_generic<double> b);

double IntensityDistance(i3d::RGB_generic<double> a, i3d::RGB b);

wxRect FixRect(wxRect rect);

wxBitmap CreateLabelButtonIcon(unsigned int label, wxColor color);


// Utility class for convenietn timing
class Timer {
    std::chrono::high_resolution_clock::time_point start;
    
public:
    Timer() {
        start = std::chrono::high_resolution_clock::now();
    }
public:
    void stop(std::string message) {
        std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();
        std::cout << message << ": " << duration*1e-6 << " seconds." << std::endl;
    }
};

#endif /* Util_hpp */
