//
//  ImagePanel.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 04.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "ImageScrolledWindow.hpp"

#include <wx/wx.h>
#include <wx/sizer.h>
#include "MainFrame.h"

BEGIN_EVENT_TABLE(ImageScrolledWindow, wxScrolledWindow)
// some useful events

 EVT_MOTION(ImageScrolledWindow::OnMouseMotion)
 EVT_LEFT_DOWN(ImageScrolledWindow::OnMouseLeftDown)
 EVT_LEFT_UP(ImageScrolledWindow::OnMouseLeftUp)
 EVT_RIGHT_DOWN(ImageScrolledWindow::OnMouseRightDown)
 EVT_LEAVE_WINDOW(ImageScrolledWindow::OnMouseLeaveWindow)

// catch paint events
EVT_PAINT(ImageScrolledWindow::OnPaint)
//Size event
EVT_SIZE(ImageScrolledWindow::OnSize)
END_EVENT_TABLE()


ImageScrolledWindow::ImageScrolledWindow(wxWindow *parent,
                                         wxWindowID winid,
                                         const wxPoint& pos,
                                         const wxSize& size,
                                         long style,
                                         const wxString& name):
wxScrolledWindow(parent, winid, pos, size, style, name) {
    _contentSize = wxSize(0,0);
	SetDoubleBuffered(true);
}


void ImageScrolledWindow::SetZoom(size_t zoom) {
    _zoom = zoom;
    SetScrollbars();
}

void ImageScrolledWindow::SetContentSize(const wxSize &size) {
    _contentSize = size;
    SetScrollbars();
}

void ImageScrolledWindow::OnPaint(wxPaintEvent & evt)
{
    wxPaintDC dc(this);
    Render(dc);
}

void ImageScrolledWindow::Render(wxDC&  dc)
{
    // Change dc's origin according to scroll position.
	PrepareDC(dc);
    
    // Draw image.
    double scale = GetZoomScale();
    dc.SetUserScale(scale, scale);
   // dc.DrawBitmap(scaled, 0, 0);
    if (_parentFrame) {
        _parentFrame->GetActiveSegmenter()->Render(dc);
    }
}


void ImageScrolledWindow::OnSize(wxSizeEvent& event){
    Refresh();
    //skip the event.
    event.Skip();
}

// MARK: - Mouse events handling

wxMouseEvent ImageScrolledWindow::AdjustEventCoordinates(wxMouseEvent &event) {
    wxMouseEvent adjusted(event);
    wxPoint position = this->CalcUnscrolledPosition( event.GetPosition() );
    position.x /= GetZoomScale();
    position.y /= GetZoomScale();
    adjusted.SetPosition(position);
    return adjusted;
}

bool ImageScrolledWindow::PointIsInsideImage(const wxPoint &point) {
    return (point.x >= 0 && point.y >=0 &&
            point.x < _contentSize.x && point.y < _contentSize.y);
}


void ImageScrolledWindow::OnMouseLeftDown(wxMouseEvent &event)
{
    if(_contentSize.GetWidth() <= 0 || _contentSize.GetHeight() <= 0) return;
    
    wxMouseEvent adjusted = AdjustEventCoordinates(event);
    bool insideImage = PointIsInsideImage(adjusted.GetPosition());
    
    if (auto segmenter = _parentFrame->GetActiveSegmenter()) {
        segmenter->OnMouseLeftDown( adjusted, insideImage );
    }

}

void ImageScrolledWindow::OnMouseLeftUp(wxMouseEvent &event)
{
    if(_contentSize.GetWidth() <= 0 || _contentSize.GetHeight() <= 0) return;
    
    wxMouseEvent adjusted = AdjustEventCoordinates(event);
    bool insideImage = PointIsInsideImage(adjusted.GetPosition());
    
    if ( auto segmenter = _parentFrame->GetActiveSegmenter() ) {
        segmenter->OnMouseLeftUp( adjusted, insideImage );
    }
}

void ImageScrolledWindow::OnMouseMotion(wxMouseEvent &event)
{
    if(_contentSize.GetWidth() <= 0 || _contentSize.GetHeight() <= 0) return;
    
    wxMouseEvent adjusted = AdjustEventCoordinates(event);
    bool insideImage = PointIsInsideImage(adjusted.GetPosition());
    if ( auto segmenter = _parentFrame->GetActiveSegmenter() ) {
        
        segmenter->OnMouseMotion( adjusted, insideImage );
    }
}

void ImageScrolledWindow::OnMouseRightDown(wxMouseEvent &event) {
    if(_contentSize.GetWidth() <= 0 || _contentSize.GetHeight() <= 0) return;
    
    wxMouseEvent adjusted = AdjustEventCoordinates(event);
    bool insideImage = PointIsInsideImage(adjusted.GetPosition());
    
    if ( auto segmenter = _parentFrame->GetActiveSegmenter() ) {
        segmenter->OnMouseRightDown( adjusted, insideImage );
    }
}

void ImageScrolledWindow::OnMouseLeaveWindow(wxMouseEvent &event) {
    if(_contentSize.GetWidth() <= 0 || _contentSize.GetHeight() <= 0) return;
    
    wxMouseEvent adjusted = AdjustEventCoordinates(event);
    if ( auto segmenter = _parentFrame->GetActiveSegmenter() ) {
        segmenter->OnMouseLeave( adjusted );
    }
}

void ImageScrolledWindow::SetScrollbars() {
    int width = _contentSize.GetWidth()*GetZoomScale();
    int height = _contentSize.GetHeight()*GetZoomScale();
    wxScrollHelperBase::SetScrollbars(1, 1, width, height);
    SetScrollRate(10, 10);
}
