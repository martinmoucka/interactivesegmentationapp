#include "MainFrame.h"

MainFrame::MainFrame(wxWindow* parent)
    : MainFrameBase(parent)
{
    wxInitAllImageHandlers();
    GetImageScrolledWindow()->SetParentFrame(this);

    auto s1 = std::make_shared<ManualSegmenter>(1, GetRibbonBar(), this);
    auto s2 = std::make_shared<RegionGrowingSegmenter>(2, GetRibbonBar(), this);
    auto s3 = std::make_shared<GraphCutSegmenter>(3, GetRibbonBar(), this);
    auto s4 = std::make_shared<GrabCutSegmenter>(4, GetRibbonBar(), this);
    auto s5 = std::make_shared<RandomWalkSegmenter>(5, GetRibbonBar(), this);
    
    // *** Create a pointer to new segmenter here.
    // auto s6 = std::make_shared<YourNewSegmenter>(6, GetRibbonBar(), this);
    
    _segmenters.push_back(s1);
    _segmenters.push_back(s2);
    _segmenters.push_back(s3);
    _segmenters.push_back(s4);
    _segmenters.push_back(s5);
    
    // *** Add the new segmenter's pointer to '_segmenters' vector here.
    // _segmenters.push_back(s6);
    
    for (auto segmenter : _segmenters) segmenter->Init();

    _activeSegmenter = s1;
    
    Bind(wxEVT_CHAR_HOOK, &MainFrame::OnKeyDown, this);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnRightMenuRepaintComponent, this, Segmenter::ID_RIGHT_CLICK_MENU_REPAINT_COMPONENT);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnRightMenuRepaintAllComponents, this, Segmenter::ID_RIGHT_CLICK_MENU_REPAINT_ALL_COMPONENTS);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnRightMenuSelectLabel, this, Segmenter::ID_RIGHT_CLICK_MENU_SELECT_LABEL);
    
    SetLabelToAllSegmenters(1);
}

MainFrame::~MainFrame()
{
    DeleteTemporaryFiles();
}


void MainFrame::OnNextImage(wxCommandEvent& event)
{
    SetImageIndex(_imageIndex + 1);
}

void MainFrame::OnPreviousImage(wxCommandEvent& event)
{
    SetImageIndex(_imageIndex - 1);
}

void MainFrame::OnOpenDirectory(wxCommandEvent& event)
{
    wxDirDialog* openDirDialog = new wxDirDialog(this, wxT("Choose a directory to open"), wxEmptyString, wxDD_DEFAULT_STYLE);
    if (openDirDialog->ShowModal() == wxID_OK) {
        DeleteTemporaryFiles();
        wxString path = openDirDialog->GetPath();
        wxArrayString* files = new wxArrayString();
        wxDir::GetAllFiles(path, files, wxEmptyString, wxDIR_FILES);
        wxArrayString* filteredFiles = new wxArrayString();
        for (size_t i = 0; i < files->GetCount(); ++i) {
            wxString ext = wxFileName(files->Item(i)).GetExt();
            wxPrintf("%s\n", ext);
            if (ext == "tga" || ext == "tif" || ext == "ics" || ext == "jpg" || ext == "png" || ext == "bmp") {
                filteredFiles->Add(files->Item(i));
            }
        }
        _imagePaths = *filteredFiles;
        for (size_t i = 0; i < _imagePaths.GetCount(); ++i) {
            wxPrintf("%s\n", _imagePaths[i]);
        }
    }
    openDirDialog->Destroy();
    
    SetImageIndex(0, false);
}

void MainFrame::OnOpenFiles(wxCommandEvent& event)
{
    wxFileDialog* openDialog = new wxFileDialog(this,wxT("Choose a file to open"), wxEmptyString, wxEmptyString, wxT("Images (*.tga;*.tif,*.ics,*.jpg,*.png,*.bmp)|*.tga;*.tif;*.ics;*.jpg;*.png;*.bmp"), wxFD_MULTIPLE);
        if (openDialog->ShowModal() == wxID_OK) {
            DeleteTemporaryFiles();
            wxArrayString paths = wxArrayString();
            openDialog->GetPaths(_imagePaths);
            for (size_t i = 0; i < _imagePaths.GetCount(); ++i) {
                wxPrintf("%s\n", _imagePaths[i]);
            }
        }
        openDialog->Destroy();
    
    SetImageIndex(0, false);
}

void MainFrame::OnKeyDown(wxKeyEvent &event) {
    switch (event.GetKeyCode()) {
        case WXK_LEFT:
            SetImageIndex(_imageIndex - 1);
            break;
        case WXK_RIGHT:
            SetImageIndex(_imageIndex + 1);
            break;
        case WXK_F1:
            SetPreviousLabelToAllSegmenters();
            break;
        case WXK_F2:
            SetNextLabelToAllSegmenters();
            break;
        case 'U':
            SetUnusedLabelToAllSegmenters();
            break;
        case 'B':
            SetLabelToAllSegmenters(0);
            break;
        case 'F':
            for (auto segmenter : _segmenters) segmenter->SetLabelDisplayMode(Segmenter::LabelsDisplayMode::Fill);
            break;
        case 'O':
            for (auto segmenter : _segmenters) segmenter->SetLabelDisplayMode(Segmenter::LabelsDisplayMode::Outline);
            break;
        case 'H':
            for (auto segmenter : _segmenters) segmenter->ToggleShowSegmentedLabels();
            break;
        default:
            GetActiveSegmenter()->OnKeyDown(event);
            break;
    }
    
}

void MainFrame::OnZoomIn(wxCommandEvent& event)
{
    if (_zoom <= 80000) { // 800% zoom
        _zoom *= 2;
        GetImageScrolledWindow()->SetZoom(_zoom);
        GetImageScrolledWindow()->Refresh();
    }
}
void MainFrame::OnZoomOut(wxCommandEvent& event)
{
    if (_zoom >= 1250) { // 12.5% zoom
        _zoom /= 2;
        GetImageScrolledWindow()->SetZoom(_zoom);
        GetImageScrolledWindow()->Refresh();
    }
}

void MainFrame::SetImageIndex(size_t index, bool saveTemporarySegmentation) {
    if ( !(index >= 0 && index < _imagePaths.GetCount()) ) {
        return;
    }
    if (saveTemporarySegmentation) SaveCurrentTemporarySegmentation();
    wxString imagePath = _imagePaths.Item(index);
    i3d::ImgVoxelType imageType = i3d::ReadImageType(imagePath);
    std::shared_ptr<SegmentedImage> segmentedImage;
    wxFileName tmpSegmentationPath = TemporarySegmentationFilePath(imagePath);
    std::cout << "Tmp seg path: " << tmpSegmentationPath.GetFullPath() << std::endl;
    if (tmpSegmentationPath.FileExists()) {
        i3d::Image3d<i3d::GRAY16> tmpSeg(tmpSegmentationPath.GetFullPath());
        if (imageType == i3d::Gray8Voxel) {
            i3d::Image3d<i3d::GRAY8> image(imagePath);
            segmentedImage = std::make_shared<SegmentedImage>(image, tmpSeg);
        }
        else if (imageType == i3d::Gray16Voxel) {
            i3d::Image3d<i3d::GRAY16> image(imagePath);
            i3d::Image3d<i3d::GRAY8> image8 = Segmenter::ConvertImage3dGRAY16ToGRAY8(image);
            segmentedImage = std::make_shared<SegmentedImage>(image8, tmpSeg);
        }
        else if (imageType == i3d::RGBVoxel) {
            i3d::Image3d<i3d::RGB> image(imagePath);
            segmentedImage = std::make_shared<SegmentedImage>(image, tmpSeg);
        } else {
            wxMessageBox("Only 8bit Grayscale or 3*8bit RGB images are supported.");
            return;
        }
    } else {
        // Check image type, load it and create SegmentedImage object.
        if (imageType == i3d::Gray8Voxel) {
            i3d::Image3d<i3d::GRAY8> image(imagePath);
            segmentedImage = std::make_shared<SegmentedImage>(image);
        }
        else if (imageType == i3d::Gray16Voxel) {
            i3d::Image3d<i3d::GRAY16> image(imagePath);
            i3d::Image3d<i3d::GRAY8> image8 = Segmenter::ConvertImage3dGRAY16ToGRAY8(image);
            segmentedImage = std::make_shared<SegmentedImage>(image8);
        }
        else if (imageType == i3d::RGBVoxel) {
            i3d::Image3d<i3d::RGB> image(imagePath);
            segmentedImage = std::make_shared<SegmentedImage>(image);
        } else {
            wxMessageBox("Only 8bit Grayscale or 3*8bit RGB images are supported.");
            return;
        }
    }
    
    // Assign loaded image to all segmenters.
    for (auto segmenter : _segmenters) {
        segmenter->SetImage(segmentedImage, std::string(imagePath.mb_str()) );
    }
    SetUnusedLabelToAllSegmenters();
    GetImageScrolledWindow()->SetContentSize(wxSize(segmentedImage->GetWidth(), segmentedImage->GetHeight()));
    _imageIndex = index;
    SetTitle(imagePath);
    Update();
}

void MainFrame::SetUnusedLabelToAllSegmenters() {
    i3d::GRAY16 maxLabel = GetActiveSegmenter()->GetMaxUsedLabel();
    if (maxLabel < std::numeric_limits<i3d::GRAY16>::max() - 1) {
        SetLabelToAllSegmenters(maxLabel + 1);
    }
}

void MainFrame::SetPreviousLabelToAllSegmenters() {
    auto label = GetActiveSegmenter()->GetLabel();
    if (label > 0) {
        SetLabelToAllSegmenters(label - 1);
    }
}

void MainFrame::SetNextLabelToAllSegmenters() {
    auto label = GetActiveSegmenter()->GetLabel();
    if (label < std::numeric_limits<i3d::GRAY16>::max() - 1) {
        SetLabelToAllSegmenters(label + 1);
    }
}

void MainFrame::SetLabelToAllSegmenters(i3d::GRAY16 label) {
    wxColour color = Segmenter::RGBToWxColour(Segmenter::ConvertLabelToDisplayColor(label));
    wxBitmap bmp = CreateLabelButtonIcon(label, color);
    for (auto& segmenter : _segmenters) {
        //set label to all segmenters
        segmenter->SetLabel(label, bmp);
    }
}

void MainFrame::OnRightMenuRepaintComponent(wxCommandEvent& event){
    GetActiveSegmenter()->OnRepaintComponent();
}
void MainFrame::OnRightMenuRepaintAllComponents(wxCommandEvent& event){
    GetActiveSegmenter()->OnRepaintAllComponents();
}
void MainFrame::OnRightMenuSelectLabel(wxCommandEvent& event){
    GetActiveSegmenter()->OnSelectLabel();
}

void MainFrame::Update() {
    GetImageScrolledWindow()->Refresh();
}
void MainFrame::OnRibbonBarPageChanged(wxRibbonBarEvent& event)
{
    auto selectedSegmenterId = event.GetPage()->GetId();
    SetActiveSegmenter(selectedSegmenterId);
}

void MainFrame::SetActiveSegmenter(int id) {
    for (auto segmenter : _segmenters) {
        if ((*segmenter).GetId() == id) {
            _activeSegmenter = segmenter;
            break;
        }
    }
    Update();
}

std::shared_ptr<Segmenter> MainFrame::GetActiveSegmenter() {
    return _activeSegmenter;
}

// MARK: - Saving segmentation

void MainFrame::OnSaveSegmentation(wxCommandEvent& event){
    SaveCurrentSegmentation();
}

void MainFrame::OnSaveAllSegmentations(wxCommandEvent& event)
{
    SaveCurrentSegmentation();
     for (size_t i = 0; i < _imagePaths.Count(); ++i) {
         if (i == _imageIndex) continue;
         wxFileName tmpSegPath = TemporarySegmentationFilePath(_imagePaths.Item(i));
         if (tmpSegPath.FileExists()) {
             wxFileName path = SegmentationFilePath(_imagePaths.Item(i));
             i3d::Image3d<i3d::GRAY16> tmpSeg(tmpSegPath.GetFullPath());
             SaveSegmentationFile(tmpSeg, path);
         }
     }
}

wxFileName MainFrame::TemporarySegmentationFilePath(wxString& imagePath) {
    wxFileName sourcePath(imagePath);
    sourcePath.AppendDir(wxT("tmp"));
    sourcePath.SetExt(wxT("png"));
    return sourcePath;
}

wxFileName MainFrame::SegmentationFilePath(wxString& imagePath) {
    wxFileName sourcePath(imagePath);
    sourcePath.AppendDir(wxT("segmented"));
    sourcePath.SetExt(wxT("png"));
    return sourcePath;
}

void MainFrame::SaveCurrentTemporarySegmentation() {
    if (GetActiveSegmenter()->GetSegmentedImage() == NULL) return;
    wxFileName path = TemporarySegmentationFilePath(_imagePaths.Item(_imageIndex));
    auto segmentation = GetActiveSegmenter()->GetLabeledImage();
    SaveSegmentationFile(segmentation, path);
}

void MainFrame::SaveCurrentSegmentation() {
    if (GetActiveSegmenter()->GetSegmentedImage() == NULL) return;
    wxFileName path = SegmentationFilePath(_imagePaths.Item(_imageIndex));
    auto segmentation = GetActiveSegmenter()->GetLabeledImage();
    SaveSegmentationFile(segmentation, path);
}

void MainFrame::SaveSegmentationFile(i3d::Image3d<i3d::GRAY16>& segmentation, wxFileName& path) {
    if (!path.DirExists()) {
        path.Mkdir();
    }
    try {
        segmentation.SaveImage(path.GetFullPath());
    } catch (i3d::IOException e) {
        std::cerr << e.what << std::endl;
        wxMessageBox(e.what);
    }
}

void MainFrame::DeleteTemporaryFiles() {
    for (size_t i = 0; i < _imagePaths.Count(); ++i) {
        wxFileName tmpSegPath = TemporarySegmentationFilePath(_imagePaths.Item(i));
        if (tmpSegPath.FileExists()) {
            wxRemoveFile(tmpSegPath.GetFullPath());
        }
    }
}

void MainFrame::OnExit(wxCommandEvent& event){
    Close(true);
}


