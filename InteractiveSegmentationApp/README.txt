Dependencies:

i3dcore
=======
http://cbia.fi.muni.cz/user_dirs/i3dlib_doc/i3dcore/index.html

i3dalgo
=======
http://cbia.fi.muni.cz/user_dirs/i3dlib_doc/i3dalgo/index.html

wxWidgets
=========
https://www.wxwidgets.org

Armadillo
=========
http://arma.sourceforge.net
Needs ARPACK: http://www.caam.rice.edu/software/ARPACK/
and SuperLU: http://crd-legacy.lbl.gov/~xiaoye/SuperLU/


Graph Cut Library
=================
http://cbia.fi.muni.cz/projects/graph-cut-library.html

