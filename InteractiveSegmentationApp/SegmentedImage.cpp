//
//  SegmentedImage.cpp
//  InteractiveSegmentationApp
//
//  Created by Martin Moučka on 08.01.18.
//  Copyright © 2018 CBIA FI MUNI. All rights reserved.
//

#include "SegmentedImage.hpp"
#include "Segmenter.hpp"

SegmentedImage::SegmentedImage(i3d::Image3d<i3d::GRAY8> &image) {
    _type = i3d::Gray8Voxel;
    _originalGrayImage = image;
    _width = image.GetSizeX();
    _height = image.GetSizeY();
    _labeledImage.MakeRoom(_width, _height, 1);
    _originalBitmap = wxBitmap(Segmenter::ConvertImage3dToWxImage(image));
    UpdateLabeledBitmap();
}

SegmentedImage::SegmentedImage(i3d::Image3d<i3d::RGB> &image) {
    _type = i3d::RGBVoxel;
    _originalRgbImage = image;
    _width = image.GetSizeX();
    _height = image.GetSizeY();
    _labeledImage.MakeRoom(_width, _height, 1);
    _originalBitmap = wxBitmap(Segmenter::ConvertImage3dToWxImage(image));
    UpdateLabeledBitmap();
}

SegmentedImage::SegmentedImage(i3d::Image3d<i3d::GRAY8> &image, i3d::Image3d<i3d::GRAY16> &segmentation) {
    _type = i3d::Gray8Voxel;
    _originalGrayImage = image;
    _width = image.GetSizeX();
    _height = image.GetSizeY();
    if (segmentation.GetWidth() == _width && segmentation.GetHeight() == _height){
        _labeledImage = segmentation;
    } else {
        _labeledImage.MakeRoom(_width, _height, 1);
    }
    _originalBitmap = wxBitmap(Segmenter::ConvertImage3dToWxImage(image));
    UpdateLabeledBitmap();
}

SegmentedImage::SegmentedImage(i3d::Image3d<i3d::RGB> &image, i3d::Image3d<i3d::GRAY16> &segmentation) {
    _type = i3d::RGBVoxel;
    _originalRgbImage = image;
    _width = image.GetSizeX();
    _height = image.GetSizeY();
    if (segmentation.GetWidth() == _width && segmentation.GetHeight() == _height){
        _labeledImage = segmentation;
    } else {
        _labeledImage.MakeRoom(_width, _height, 1);
    }
    _originalBitmap = wxBitmap(Segmenter::ConvertImage3dToWxImage(image));
    UpdateLabeledBitmap();
}

void SegmentedImage::AddLabels(i3d::Image3d<i3d::GRAY16> &image) {
    if (image.GetWidth() != _width || image.GetHeight() != _height) {
        return;
    }

    auto oldIt = _labeledImage.begin();
    for (auto newIt = image.begin(); newIt != image.end(); ++newIt, ++oldIt) {
        // If pixel is zero, ignore it
        if (*newIt != 0) {
            // If pixel is max of GRAY16, it means background
            if(*newIt == std::numeric_limits<i3d::GRAY16>::max()) {
                *oldIt = 0;
            } else {
                *oldIt = *newIt;
            }
        }
    }
    
    // Update labeled bitmap.
    UpdateLabeledBitmap();
}

void SegmentedImage::AddBinaryLabel(i3d::Image3d<i3d::GRAY16> &image, i3d::GRAY16 label) {
    
    auto oldIt = _labeledImage.begin();
    for (auto newIt = image.begin(); newIt != image.end(); ++newIt, ++oldIt) {
        if (*newIt == Segmenter::BinaryLabelType::Foreground) {
            *oldIt = label;
        }
    }
    
    UpdateLabeledBitmap();
}

i3d::Image3d<i3d::RGB> SegmentedImage::GetOriginalImage() {
    if (_type == i3d::RGBVoxel) {
        return _originalRgbImage;
    } else {
        return i3d::Image3d<i3d::RGB>().ConvertFrom(_originalGrayImage);
    }
}

i3d::Image3d<i3d::RGB> SegmentedImage::GetOriginalRgbImage() {
    return _originalRgbImage;
}

i3d::Image3d<i3d::GRAY8> SegmentedImage::GetOriginalGrayImage() {
    return _originalGrayImage;
}

i3d::Image3d<i3d::GRAY16> SegmentedImage::GetLabeledImage() {
    return _labeledImage;
}

wxBitmap SegmentedImage::GetOriginalBitmap() {
    return _originalBitmap;
}

wxBitmap SegmentedImage::GetLabeledBitmap() {
    return _labeledBitmap;
}

wxBitmap SegmentedImage::GetLabeledOutlinedBitmap() {
    return _labeledOutlinedBitmap;
}

void SegmentedImage::ComputeLabeledOutlineBitmap(i3d::Image3d<i3d::GRAY16> &image) {

    i3d::Image3d<i3d::GRAY16> out; out.MakeRoom(image.GetWidth(), image.GetHeight(), 1);
    out.SetAllVoxels(0);
    for (size_t y = 0; y < image.GetHeight(); ++y) {
        for (size_t x = 0; x < image.GetWidth(); ++x) {
            auto label = image.GetVoxel(x, y, 0);
            if (label == 0) continue;
            bool isBorder = false;
            
            if (x == 0 || y == 0 || x == image.GetWidth()-1 || y == image.GetHeight()-1){
                isBorder = true;
            } else {
                for (int dy = -1; dy <= 1; ++dy) {
                    for (int dx = -1; dx <= 1; ++dx) {
                        int ny = (int)y+dy, nx = (int)x+dx;
                        if (nx >=0 && nx < image.GetWidth() && ny >= 0 && ny < image.GetHeight()) {
                            if (image.GetVoxel(nx, ny, 0) != label) isBorder = true;
                        }
                    }
                }
            }
    
            if (isBorder) {
                out.SetVoxel(x, y, 0, label);
            }
        }
    }
    wxImage img = Segmenter::ConvertLabelImage3dToDisplayWxImage(out);
    img.SetMaskColour(0, 0, 0);
    _labeledOutlinedBitmap = wxBitmap(img);
}

void SegmentedImage::UpdateLabeledBitmap() {
    wxImage img = Segmenter::ConvertLabelImage3dToDisplayWxImage(_labeledImage);
    img.SetMaskColour(0, 0, 0);
    _labeledBitmap = wxBitmap(img);
    UpdateLabeledOutlineBitmap();
}

void SegmentedImage::UpdateLabeledOutlineBitmap() {
    ComputeLabeledOutlineBitmap(_labeledImage);
}

void SegmentedImage::RepaintComponent(int x, int y, i3d::GRAY16 label) {
    i3d::FloodFill(_labeledImage, label, x, y, 0);
    UpdateLabeledBitmap();
}

void SegmentedImage::RepaintAllComponents(int x, int y, i3d::GRAY16 label) {
    auto oldLabel = _labeledImage.GetVoxel(x, y, 0);
    for (size_t i = 0; i < _labeledImage.GetImageSize(); ++i) {
        if (_labeledImage.GetVoxel(i) == oldLabel) {
            _labeledImage.SetVoxel(i, label);
        }
    }
    UpdateLabeledBitmap();
}

SegmentedImage::~SegmentedImage() {
    
}
